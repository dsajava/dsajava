package stackoverflow.serialization;

import java.io.Serializable;

public class Employee implements Serializable {

	private static final long serialVersionUID = -5081357593958334934L;

	public String name;
	public String address;
	public transient int SSN;
	public int number;
	
	Employee() {
		name = "DROY";
		address = "Mumbai, India";
		SSN = 999999999;
		number = 1;
	}
	
	public void mailCheck() {
		System.out.println("Mailing a check to " + name + " " + address);
	}
	
}
