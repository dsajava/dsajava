package stackoverflow.serialization;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DeserializeDemo {

	public static void main(String[] args) {
		Employee e = null;
		
		try {
			FileInputStream fileIn = new FileInputStream("/Users/diptanealroy/shadows/dsajava/stackoverflow/data/employee.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			
			e = (Employee)in.readObject();
			in.close();
			fileIn.close();
			
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException i) {
			i.printStackTrace();
		} catch (ClassNotFoundException c) {
			c.printStackTrace();
		}
		
		System.out.println("Employee class has been deserialized");
		
		if (e != null) {
			System.out.println("Name " + e.name);
			System.out.println("Address " + e.address);
			System.out.println("Address " + e.SSN); // transient field - not serialized
			System.out.println("Number " + e.number);
		}
		
	}
}
