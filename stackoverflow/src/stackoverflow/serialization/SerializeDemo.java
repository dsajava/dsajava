package stackoverflow.serialization;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class SerializeDemo {
	
	public static void main(String[] args) {
		Employee e = new Employee();
		e.name = "Romeo";
		e.address = "Alpha Beta Gamma";
		e.number = 777;
		e.SSN = 777777777;
		
		try {
			FileOutputStream fileOut = new FileOutputStream("/Users/diptanealroy/shadows/dsajava/stackoverflow/data/employee.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(e);
			out.close();
			fileOut.close();
			System.out.println("Serialized file has been created in the location specified.");
			
		}
		catch (IOException i) {
			i.printStackTrace();
		}
	}

}
