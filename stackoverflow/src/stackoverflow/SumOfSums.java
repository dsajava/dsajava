package stackoverflow;
import static org.junit.Assert.assertTrue;

public class SumOfSums {

	public int sumOfSums(int n) {
		int[] numArray = new int[n];
		int totalSum = 0;
		int counter = 0;
		
		for (int i = 0; i < n ; i++){
			numArray[i] = n;
			n--;
			System.out.println(numArray[i]);
		}
		
		for (int j = 0; j < n; j++){
			totalSum += numArray[j];
			System.out.println("I am in for loop");
			if (j == n-1){
				j = 0;
				counter++;
			}
			if (counter == n){
				return totalSum;
			}

		}
		return totalSum;
	}
	
	public boolean isPrime(int n) {
		// Corner case 
        if (n <= 1) return false; 
      
        // Check from 2 to n-1 
        for (int i = 2; i < n; i++) 
            if (n % i == 0) 
                return false; 
      
        return false;
	}

	public static void main(String[] args){
		SumOfSums hm=new SumOfSums();
		assert hm.isPrime(-37337);

        assert hm.sumOfSums(1) == 1;
        assert hm.sumOfSums(3) == 10;
        assert hm.sumOfSums(6) == 56;
        assert hm.sumOfSums(25) == 2925;
        assert hm.sumOfSums(-5) == 0;


		try {
	        assertTrue(hm.isPrime(-37337));
		}
		catch (AssertionError e ) {
			e.printStackTrace();
		}
		assert (hm.sumOfSums(1) == 1);
		
		System.out.println("All tests passed.  VICTORY!");
	}

}
