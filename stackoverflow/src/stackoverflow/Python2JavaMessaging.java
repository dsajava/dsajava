package stackoverflow;
import java.io.*;

public class Python2JavaMessaging {
	public static void main(String[] args) {
		try {
			BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
			PrintWriter writer = new PrintWriter("result.txt", "UTF-8");
			String s = bufferRead.readLine();
			while(s.equals("x")==false) {
				writer.println(s);
				s = bufferRead.readLine();
				System.out.println("Writing a line" + s);
			}
			writer.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
}



