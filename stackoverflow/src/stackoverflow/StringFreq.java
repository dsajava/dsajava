package stackoverflow;

public class StringFreq {

    private static int[] decodeFrequency1(String context) {
        int[] freqArr = new int[127];
        for ( char c : context.toCharArray() ) {
            freqArr[(int)c]++;
        }
        return freqArr;
    }

    private static int[] decodeFrequency2(String context) {
        int[] freqArr = new int[127];
        for ( char c : context.toCharArray() ) {
        	freqArr[c - 'A']++;
        }
        return freqArr;
    }

    private static void printAll(int[] array) {
    	System.out.println();
    	for (int i=0; i < array.length; i++) {
    		if ( array[i] !=0 )
    			System.out.print(Character.toString((char) i) + " " + array[i] + " ");
    	}
    	System.out.println();
    }

	public static void main(String[] args) {
		String name = "Romeo";
		String email = "romeo@alphacharlie.com";
		
		
		System.out.print("Character Freq count for name using method 1 :");
		printAll(decodeFrequency1(name));
		
		System.out.print("Character Freq count for email using method 1 :");
		printAll(decodeFrequency1(email));

		System.out.print("Character Freq count for name using method 2 :");
		printAll(decodeFrequency2(name));
		
		System.out.print("Character Freq count for email using method 2 :");
		printAll(decodeFrequency2(email));

	}
}
