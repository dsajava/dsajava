package stackoverflow;

public class PrintOddEvenUsingInterrupt {    

public static volatile int count;
public volatile static boolean oddFinished = false;
public volatile static boolean evenFinished = false;


static class OddInterruptThread implements Runnable {
    public void run() {
        int oldNum = 0;//Break points doesn't works here
        while (! oddFinished) {
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
            }

            if (oldNum != count && count % 2 == 1) {
                System.out.println(Thread.currentThread().getName()
                        + " prints " + count);
                oldNum = count;
            }
        }
    }
}

static class EvenInterruptThread implements Runnable {
    public void run() {
        int oldNum = 0;//Break points doesn't works here
        while (! evenFinished) {
            try {
                 Thread.sleep(1000L);
            } catch (InterruptedException e) {
            }

            if (oldNum != count && count % 2 == 0) {
                System.out.println(Thread.currentThread().getName()
                        + " prints " + count);
                oldNum = count;
            }
        }
    }
}

public static void main(String[] args) throws InterruptedException {
    Thread oddThread = new Thread(new OddInterruptThread(), "Odd Thread ");
    Thread evenThread = new Thread(new EvenInterruptThread(),"Even Thread ");

    oddThread.start();
    evenThread.start();
    for (int i = 0; i < 3; i++) {
        count++;
        oddThread.interrupt();//Break points works here 
        evenThread.interrupt();
        Thread.sleep(1000L);// Line-a 
    }
    
    oddFinished = true;
    evenFinished = true;
    
    oddThread.join();
    evenThread.join();
}

}    