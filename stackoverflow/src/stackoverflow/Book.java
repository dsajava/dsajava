package stackoverflow;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public final class Book {

	private final Path p;
	
	Book() {
		// p = Paths.get("/Users/diptanealroy/README.txt");
		p = Paths.get("/Users/diptanealroy/PythonGenerated.txt");
	}
	
	public String content() throws IOException {
		Path x = this.p;
		return new String(Files.readAllBytes(x));
	}
	
		
	public static void main(String[] args) {
		try {
			Book b = new Book();
			//String content;
			//content = b.content();
			//System.out.println(content);
			
	        File inputFile = new File(b.p.toString());
	        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(inputFile),"UTF-8"));
	        String output = "";
	        String line;
	        while ((line = br.readLine()) != null) {
	           // This block never hits when invoked by python. It works fine when java program runs directly.
	           output +=line+" ";
	         }
	        
	        System.out.println(output);
	        br.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
