package stackoverflow.effectivejava;

public class TestSingletonClass {
	
	public static void main(String[] args) {
		SingletonClass instance = SingletonClass.INSTANCE;
		instance.printData();
		instance.printData();
		SingletonClass instance2 = SingletonClass.INSTANCE;
		instance2.printData();
		SingletonClass instance3 = SingletonClass.INSTANCE;
		instance3.printData();
		instance3.printData();

	}

}
