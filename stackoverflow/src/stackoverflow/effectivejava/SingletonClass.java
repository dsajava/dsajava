package stackoverflow.effectivejava;

import java.util.Random;

public class SingletonClass {
	public static final SingletonClass INSTANCE = new SingletonClass();
	
	private int _data = 10;
	
	private SingletonClass() {
		Random number = new Random();
		_data = number.nextInt(100);
	}
	
	public void printData() {
		System.out.println(_data);
	}
}
