package stackoverflow.jsoup;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class DemoProgram {

	public static void demoFunction() {
		Document doc;
		try {
			doc = Jsoup.connect("http://en.wikipedia.org/").get();
			System.out.println(doc.title());
			Elements newsHeadlines = doc.select("#mp-itn b a");
			for (Element headline : newsHeadlines) {
				System.out.println("%s\n\t%s" +  headline.attr("title") + headline.absUrl("href"));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		demoFunction();
	}
	
}
