package stackoverflow.streams;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReadStreamFile {
	public static final String COMMA = ",";

	
	public static void readInputFile() {
		final String fileName = "/Users/diptanealroy/dummy.txt";
		List<String> list = new ArrayList<>();
		
		while (true) {
			try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
				list = stream
						.filter(line -> !line.startsWith("I"))
						.map(String::toUpperCase)
						.collect(Collectors.toList());
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			list.forEach(System.out::println);
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void main(String[] args) {
		readInputFile();
	}

}
