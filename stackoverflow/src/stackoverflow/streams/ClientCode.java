package stackoverflow.streams;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.InetAddress;
import java.net.Socket;


public class ClientCode {
	
	private void startClient() {
	    DataInputStream in = null;
	    DataOutputStream out = null;
	    Socket socket = null;
	    try {
		    InetAddress host = InetAddress.getLocalHost();
	        socket = new Socket(host.getHostName(), 9876);

	        in = new DataInputStream(socket.getInputStream());
	        out = new DataOutputStream(socket.getOutputStream());
	        
	        //MANDAMOS EL NUMERO EN RANGO HACIA LOS SERVIDORES
	        out.writeInt(1000);
	        out.flush();
	        
	        //LEEMOS EL TIEMPO ENVIADO POR EL SERVIDOR
	        long tiempo = in.readLong();
	        System.out.println(tiempo);
	        String str;
	        ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
	        if ((str = (String) ois.readObject()) != null) {
	            System.out.println(str);
	        }
	        ois.close();

	    } catch (IOException ex) {
	    	ex.printStackTrace();
	    } catch (ClassNotFoundException ex) {
	    	ex.printStackTrace();
	    } finally {
	        try {
	            out.close();
	            in.close();
	            socket.close();
	        } catch (IOException ex) {
	        	ex.printStackTrace();
	        }
	    }
	}

	public static void main(String[] args) {
		ClientCode cc = new ClientCode();
		cc.startClient();
	}
}
