package stackoverflow.streams;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;


public class ServerCode {

	private void startServer(){

	    DataOutputStream out = null;
	    DataInputStream in = null;
	    ServerSocket ss = null;
	    try {

	        Socket socket = null;
	        ss = new ServerSocket(9876);
	        System.out.println("Esperando conexion");
	        socket = ss.accept();
	        in = new DataInputStream(socket.getInputStream());
	        int n = in.readInt();
	        System.out.println(n);
	        long time = 9000L;
	        out = new DataOutputStream(socket.getOutputStream());
	        out.writeLong(time);
	        out.flush();

	        ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
	        String c = "My name is Mr.X";
	        oos.writeObject(c);

	        oos.close();
	        in.close();
	        out.close();
	        socket.close();
	    } catch (IOException ex) {
	    	ex.printStackTrace();
	    } finally {
	    }
	}
	
	public static void main(String[] args) {
		ServerCode sc = new ServerCode();
		sc.startServer();
	}
}
