package stackoverflow.streams;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class TimeServer {

	public static void main(String[] args) throws InterruptedException {
		Socket socket;
		try {
			String content = "Hello World. How are you? ";
			byte[] bytes = content.getBytes();

			socket = new Socket("127.0.0.1", 9876);
			OutputStream out = socket.getOutputStream();

	        for (int i=0; i < 100000; i++) {
	        	out.write(bytes);
	        	Thread.sleep(1000);
	        }
	        
			out.flush();
			out.close();
			socket.close();

		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
