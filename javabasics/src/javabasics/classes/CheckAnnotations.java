package javabasics.classes;

import java.util.ArrayList;

class A {
	public void showThisDatabase() {
		System.out.println("in A");		
	}
}

class B extends A {
	
	
	@Override
	public void showThisDatabase() {
		@SuppressWarnings("unused")
		ArrayList<Integer> obj = new ArrayList<Integer>();
		System.out.println("In B");
	}
}

public class CheckAnnotations {

	public static void main(String[] args) {
		A obj = new B();
		obj.showThisDatabase();
	}
	
}
