package javabasics.classes;

import java.util.ArrayList;

public class AccessorMutator {
	
	public static void main(String[] args) {
		ArrayList<String> friends = new ArrayList<>();
		friends.add("Peter");
		
		ArrayList<String> people = friends;
		people.add("Paul");
		
		for (String person : people) {
			System.out.println("Name of the person is: " + person);
		}
	}

}
