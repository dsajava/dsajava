package javabasics.classes;

public class Box<T> {
	
	private T t;
	
	public void set(T t) {
		this.t = t;
	}
	
	public T get() {
		return t;
	}
	
	public <U extends Number> void inspect(U u) {
		System.out.println("T :" + t.getClass().getName());
		System.out.println("U :" + u.getClass().getName());
	}
	
	public static <E extends Comparable<E>> int countGreaterThan(E[] array, E elem) {
		int count = 0;
		for (E e: array) {
			if (e.compareTo(elem) > 0) {
				count++;
			}
		}
		return count;
	}
	
	public static void main(String[] args) {
		Box<Integer> box = new Box<>();
		box.set(new Integer(10));
		box.inspect(90);
		
		Integer[] array = {3, 4, 5, 6};
		Integer number = 4;
		System.out.println("Number of elements in the array greater than " + number + " is " + countGreaterThan(array, number));
	}
}
