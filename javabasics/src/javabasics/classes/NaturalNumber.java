package javabasics.classes;

public class NaturalNumber<T extends Number> {
	
	private T n;
	
	
	public NaturalNumber(T n) {
		this.n = n;
	}
	
	public boolean isEven() {
		return n.intValue() % 2 == 0;
	}
	
	public static void main(String[] args) {
		NaturalNumber<Double> nn = new NaturalNumber<>(23.5);
		System.out.println(nn.isEven());
		
	}

}
