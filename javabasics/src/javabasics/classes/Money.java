package javabasics.classes;

public class Money {
	private final int amount;
	private final String currency;
	
	public Money(int amount, String currency) {
		this.amount = amount;
		this.currency = currency;
	}
	
	public int getAmount() {
		return amount;
	}
	
	public String getCurrency() {
		return currency;
	}
	
	public boolean equals(Object obj) {
		if (obj instanceof Money) {
			Money money = (Money) obj;
			return getAmount() == money.getAmount() && getCurrency().equals(money.getCurrency());
		}
		return false;
	}
}
