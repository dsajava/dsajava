package javabasics.classes;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

class MoneyTest {

	@Test
	public void constructorShouldSetAmountAndCurrency() {
		Money money = new Money(100, "Dollars");
		assertEquals(100, money.getAmount());
		assertEquals("Dollars", money.getCurrency());
	}
	
}
