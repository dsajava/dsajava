package queues;

/*
 * Circular queue can handle the implementation of queue where 
 * we can enqueue elements until the capacity is reached.
 * If dequeue is done, the capacity is readjusted
 */
public class CircularQueue {
    int[] queue;
    final static int CAPACITY = 5;

    int front = -1;
    int rear = -1;
    
    CircularQueue () {
        queue = new int[CAPACITY];
    }
    /*
     * enqueue ensures we increment the rear pointer and add elements at rear
     * It will check if the queue is full comparing the front pointer position
     * If the front pointer is not at the starting position, we would have dequeued 
     * few elements and so, it will reset the rear pointer and add elements from 0 again
     */
    public void enqueue(int element) {
        System.out.println("Before Enqueue: " + element 
        		+ " queue.length is: " + queue.length 
        		+ " Front is: " + front 
        		+ " Rear is: " + rear);
       
        // if size is full then indicate it is full
        if ( rear == queue.length - 1  && front == -1 || (front != -1 && rear == front)) {
            System.out.println("Queue is full");
            return;
        }
        else if ( rear == queue.length -1 ) {
            rear = 0;
            queue[rear] = element;
        }
        else {        
            queue[++rear] = element;
        }
    }
    
    /*
     * dequeue removes element from front pointer position
     * It checks, if the queue is empty.
     * It also smartly checks if the front has reached the end of the queue
     * capacity, in which case it resets the front to -1
     * It further checks if the queue front and rear are at the same location
     * which implies, that the queue is empty and we can reset both the pointers to -1
     */
    public int dequeue() {
        System.out.println("Dequeue Front is:" + front + " Rear is: " + rear);
        
        if ( rear == -1 ) {
            System.out.println("Queue is empty");
            return -1;
        }
        
        int tmp = queue[++front];
        queue[front] = -1;
        
        if ( front == (queue.length - 1)){
            front = -1;
        }
        else if ( front == rear){
            front = -1;
            rear = -1;
        }
        return tmp;
    }
    
    public static void main(String[] args) {
        CircularQueue queue = new CircularQueue();
        try {
        	queue.dequeue();
            queue.enqueue(5);
            queue.enqueue(6);
            queue.enqueue(7);
            queue.enqueue(15);
            queue.enqueue(20);
            queue.enqueue(9);
            queue.enqueue(10);
            System.out.println("Dequeuing..." + queue.dequeue());
            System.out.println("Dequeuing..." + queue.dequeue());
            queue.enqueue(21);
            queue.enqueue(34);
            System.out.println("Dequeuing..." + queue.dequeue());
            System.out.println("Dequeuing..." + queue.dequeue());
            System.out.println("Dequeuing..." + queue.dequeue());
            System.out.println("Dequeuing..." + queue.dequeue());
            System.out.println("Dequeuing..." + queue.dequeue());
            System.out.println("Dequeuing..." + queue.dequeue());
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}