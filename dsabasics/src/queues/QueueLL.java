package queues;

class Node {
	private int _data;
	private Node _next;
	
	Node(int data) {
		_data = data;
		_next = null;
	}
	
	public Node getNext() {
		return _next;
	}
	
	public void setNext( Node node ) {
		_next = node;
	}
	
	public int getData() {
		return _data;
	}
	
	public void setData( int data ) {
		_data = data;
	}
}

public class QueueLL {
    Node rear, front;
    
	public void enqueue (int data){
        Node node = new Node(data);
        if ( rear == null ) {
            rear = node;
            front = node;
            return;
        }
         
        System.out.println("Node with data " + data + " is inserted.");
        rear.setNext(node);
        rear = rear.getNext();
    }
    
    public int dequeue() {
        if ( front == null ) {
            System.out.println("Queue is empty");
            return -1;
        }
        
        int tmp = front.getData();
        front = front.getNext();
        if ( front == null ) {
            rear = null;
        }
        return tmp;
    }

    public static void main(String[] args) {
        QueueLL queue = new QueueLL();
        queue.enqueue(10);
        queue.enqueue(20);
        System.out.println("Dequeue..." + queue.dequeue());
        System.out.println("Dequeue..." + queue.dequeue());
        queue.enqueue(90);
        System.out.println("Dequeue..." + queue.dequeue());
    }
    
}