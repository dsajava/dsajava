package queues;

import stacks.EmptyStackException;
import stacks.FullStackException;

/*
 * Queue implementation using array
 * Implementation has issue where dequeue has no effect on the capacity
 * 
 */

public class BasicQueue {
	static final int QUEUE_CAPACITY = 10;
	int capacity;
	int front;
	int rear;
	
	int[] queue;
	
	public BasicQueue(){
		this(QUEUE_CAPACITY);
	}
	
	public BasicQueue(int capacity) {
		this.capacity = capacity;
		queue = new int[capacity];
	}
	
	/*
	 * This returns the number of elements in the queue
	 */
	public int size() {
		return rear;
	}
	
	public boolean isEmpty() {
		return rear == -1 && front == -1;
	}
	
	public void enqueue (int element) throws FullStackException {
		if (size() == capacity) {
			throw new FullStackException("Queue is full");
		}
		else if ( isEmpty() ) {
			rear = front = 0;
		}

		queue[rear++] = element;
	}
	
	public boolean isFull() {
		return capacity == rear;
	}
	
	public int dequeue() throws EmptyStackException {
		if (isEmpty()) {
			throw new EmptyStackException("Queue is empty");
		}
		else if ( front == rear ) {
			front = rear = -1;
			throw new EmptyStackException("Queue is empty");
		}
		else { 
			int tmp = queue[front];
			queue[front++] = -1;
			return tmp;			
		}
	}
	
	public static void main(String[] args) {
		try {
			BasicQueue bs = new BasicQueue(3);
			bs.enqueue(10);
			bs.enqueue(30);
			bs.enqueue(40);
			System.out.println("Dequeue: " + bs.dequeue());
			System.out.println("Dequeue: " + bs.dequeue());
			System.out.println("Dequeue: " + bs.dequeue());

			bs.enqueue(90);
			System.out.println(bs.isFull());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
