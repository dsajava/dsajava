package queues;

/*
 * Queue implementation using array
 * adjust the queue
 * 
 */

public class Queue {

	int[] queue;
    int front = -1;
    int rear = -1;
    int capacity;
    
	static final int QUEUE_CAPACITY = 10;

    
    Queue() {
    	this.capacity = QUEUE_CAPACITY;
        queue = new int[capacity];
    }

    Queue(int c) {
    	this.capacity = c;
        queue = new int[capacity];
    }
    
    public void enqueue(int element) throws Exception {
        // if size is full then indicate it is full
        if ( rear == queue.length - 1  ) {
            throw new Exception("Queue is full");
        }
        queue[++rear] = element;
    }
    
    public boolean isEmpty() {
        return rear == front;
    }
    
    public int dequeue() throws Exception {
        if ( isEmpty() ) {
            throw new Exception("Queue is empty");
        }
        
        int tmp = queue[0];
        
        // shift all elements from index 2 till rear to front
        for ( int i=0; i < rear - 1; i++) {
        	queue[i] = queue[i+1];
        }
        
        if ( rear <= this.capacity ) {
        	queue[rear] = -1;
        }
        
        rear--;
        return tmp;
    }
    
    public static void main(String[] args) {
        Queue queue = new Queue(3);
        try {
            queue.enqueue(10);
            queue.enqueue(20);
            queue.enqueue(30);
            System.out.println("Dequeuing..." + queue.dequeue());
            System.out.println("Dequeuing..." + queue.dequeue());
            System.out.println("Dequeuing..." + queue.dequeue());
            queue.enqueue(40);
            System.out.println("Dequeuing..." + queue.dequeue());
            queue.enqueue(10);
            queue.enqueue(20);
            queue.enqueue(30);
            System.out.println("Dequeuing..." + queue.dequeue());
            System.out.println("Dequeuing..." + queue.dequeue());
            System.out.println("Dequeuing..." + queue.dequeue());
            System.out.println("Dequeuing..." + queue.dequeue());


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}