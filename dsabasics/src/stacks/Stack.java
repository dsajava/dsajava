package stacks;

public interface Stack {
	
	/*
	 * Returns number of elements in the stack
	 * @return number of elements in the stack
	 */
	public int size();
	
	/*
	 * Return whether the stack is empty
	 * @return true if the stack is empty
	 */
	public boolean isEmpty();
	
	/*
	 * Inspect the element at the top of the stack
	 * @return top element in the stack
	 * @exception EmptyStackException
	 */
	public Object peek() throws EmptyStackException;
	
	/*
	 * Removes the top element from the Stack
	 * @return element removed
	 * @exception EmptyStackException
	 */
	public Object pop() throws EmptyStackException;

	/*
	 * Inserts an element on top of the stack
	 * @param element element to be inserted
	 */
	public void push(Object element) throws FullStackException;
}
