package stacks;

public class BasicStack implements Stack {
	
	final static int CAPACITY_STACK = 100;
	int top = -1;
	int capacity;
	Object[] _stack;

	public BasicStack() {
		this(CAPACITY_STACK);
	}
	
	public BasicStack(int capacity){
		this.capacity = capacity;
		_stack = new Object[capacity];
	}
	
	@Override
	public int size() {
		return top + 1;
	}

	/*
	 * Return whether the stack is empty
	 * @return true if the stack is empty
	 */
	@Override
	public boolean isEmpty() {
		return (top < 0);
 	}

	@Override
	public Object peek() throws EmptyStackException {
		if (isEmpty()) {
			throw new EmptyStackException("Stack is empty");
		}
		
		return _stack[top];
	}

	@Override
	public Object pop() throws EmptyStackException {
		if (isEmpty()) {
			throw new EmptyStackException("Stack is empty");
		}
		
		Object tmp = _stack[top];
		_stack[top--] = null;
 		return tmp;
	}

	@Override
	public void push(Object element) throws FullStackException {
		if (size() == capacity){
			throw new FullStackException("Stack is full");
		}
		
		_stack[++top] = element;
	}

	public static void main(String[] args){
		BasicStack bs = new BasicStack(5);
		
		bs.push("Hello");
		bs.push("World");
		bs.push("How");
		bs.push("are");
		bs.push("you");
		System.out.println("Inserted five elements");
		
		try {
			bs.push("?");
		}
		catch (FullStackException e) {
			e.printStackTrace();
		}

		System.out.println("Popping the element:" + bs.pop());
		System.out.println("Peeking element at top:" + bs.peek());
		System.out.println("Size of the stack is:" + bs.size());
	}
}
