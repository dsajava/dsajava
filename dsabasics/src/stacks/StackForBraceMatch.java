package stacks;

public class StackForBraceMatch {
    private int top = -1;
    private static int CAPACITY = 10;
    private Object [] array;
    
    StackForBraceMatch() {
        this(CAPACITY);        
    }
    
    StackForBraceMatch( int capacity ) {
        array = new Object[capacity];
    }
    
    public void push( Object item ) throws Exception {
        if ( top + 1 == array.length ) {
            throw new Exception("Stack is full");
        }
        
        array[++top] = item;
    }
    
    public Object pop() throws Exception {
        if ( top == -1 ) {
           throw new Exception("Stack is empty"); 
        }
        
        Object tmp = array[top];
        array[top--] = null;
        return tmp;
    }
    
    boolean isEmpty() {
        return top == -1;
    }
    
    public boolean isBalancedStream ( String s) throws Exception {
        for ( int i = 0; i < s.length(); i++) {
            String sub = s.charAt(i) + "";
            if ( sub.equals("{") || sub.equals("[") || sub.equals("(") ){
                push(sub);
            }
            
            if ( sub.equals("}") || sub.equals("]") || sub.equals(")")){
                Object poppedVal = pop();
                if ( ! poppedVal.equals(sub)) {
                    return false;
                }
            }
        }
        return true;
    }
    
    public static void main(String[] args ) {
        StackForBraceMatch stack = new StackForBraceMatch();
        
        String a = "{]{[";
        String b = "[{}]";
        
        try {
            System.out.println( a + " is this a balanced stream " + stack.isBalancedStream(a));
            System.out.println( b + " is this a balanced stream " + stack.isBalancedStream(b));
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }   
}