package stacks;

class QNode {
	private int _data;
	private QNode _next;
	
	QNode(int data) {
		_data = data;
		_next = null;
	}
	
	public QNode getNext() {
		return _next;
	}
	
	public void setNext( QNode QNode ) {
		_next = QNode;
	}
	
	public int getData() {
		return _data;
	}
	
	public void setData( int data ) {
		_data = data;
	}
}

class BasicQueue {
    QNode rear, front;
    
    public int size() {
        QNode current = front;
        
        int size = 0;
        while( current != null ) {
            size++;
            current = current.getNext();
        }
        return size;
    }
    
	public void enqueue (int data){
        QNode QNode = new QNode(data);
        if ( rear == null ) {
            rear = QNode;
            front = QNode;
            return;
        }
         
        rear.setNext(QNode);
        rear = rear.getNext();
    }
    
    public int dequeue() {
        if ( front == null ) {
            System.out.println("Queue is empty");
            return -1;
        }
        
        int tmp = front.getData();
        front = front.getNext();
        if ( front == null ) {
            rear = null;
        }
        return tmp;
    }
}

public class StackUsingQueue {
    BasicQueue queue1;
    BasicQueue queue2;
    
    StackUsingQueue() {
        queue1 = new BasicQueue();
        queue2 = new BasicQueue();
    }
    
    public void push( int data ) {
        queue1.enqueue(data);
    }
    
    public int pop() {
        if ( queue1.size() == 0){
            System.out.println("Stack is empty");
            return -1;
        }
        
        while ( queue1.size() > 1) {
            queue2.enqueue(queue1.dequeue());            
        }
        
        int tmp = queue1.dequeue();
        
        BasicQueue q = queue1;
        queue1 = queue2;
        queue2 = q;
        return tmp;
    }
    
    public static void main(String[] args) {
        StackUsingQueue stack = new StackUsingQueue();
        stack.push(10);
        stack.push(20);
        System.out.println("Popping..." + stack.pop());
        System.out.println("Popping..." + stack.pop());
        System.out.println("Popping..." + stack.pop());
        stack.push(30);
        System.out.println("Popping..." + stack.pop());
        stack.push(40);
    }
}
