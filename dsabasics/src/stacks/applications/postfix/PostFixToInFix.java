package stacks.applications.postfix;

class MyStackImpl {
    private int top = -1;
    private static int CAPACITY = 10;
    private String [] array;
    
    MyStackImpl() {
        this(CAPACITY);        
    }
    
    MyStackImpl( int capacity ) {
        array = new String[capacity];
    }
    
    public void push( String item ) throws Exception {
        if ( top + 1 == array.length ) {
            throw new Exception("Stack is full");
        }
        
        array[++top] = item;
    }
    
    public String pop() throws Exception {
        if ( top == -1 ) {
           throw new Exception("Stack is empty"); 
        }
        
        String tmp = array[top];
        top--;
        return tmp;
    }
    
    public String top() {
        return array[top];    
    }   
    
    
    public boolean isEmpty() {
        return top < 0;
    }
}

public class PostFixToInFix {

    public static boolean isOperator( char c) {
	    if ( c == '+' || c == '-' || c == '/' || c == '*' || c == '^' || c == '(' || c == ')' ) {
	        return true;
	    }
	    return false;
	}
	
	public static String postFixToInFixConvertor ( String in) throws Exception {
	    MyStackImpl stack = new MyStackImpl();
	    
	    // iterate through the inbound expression
	    for ( int i = 0; i < in.length(); i++) {
	        char ch = in.charAt(i);
            // check if the char is an operator
            // if it is operator then pop the stack twice and form the expression
            // push the expression back to the stack ~ enclose in parenthesis
	        if ( isOperator( ch ) ){
	            String operand1 = stack.pop();
	            String operand2 = stack.pop();
	            
	            String expr = "(" + operand2 + ch + operand1 + ")";
	            stack.push(expr);
	        }
	        else {
	            // if the char is not an operator, then push it on the stack
	            stack.push(ch+"");
	        }
	    }
	    // return the top of the stack for output
	    return stack.pop();
	}

    public static void main(String[] args) {
        try {
            String s1 = "ab+";
            String s2 = "ab*c+";
            
            System.out.println("Output of Post 2 Infix conversion for " + s1 + " is " + postFixToInFixConvertor(s1));
            System.out.println("Output of Post 2 Infix conversion for " + s2 + " is " + postFixToInFixConvertor(s2));
        }
        catch (Exception e) {
            
        }
    }
}