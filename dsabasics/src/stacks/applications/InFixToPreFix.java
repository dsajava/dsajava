package stacks.applications;

public class InFixToPreFix {
    
 	private static int precedence ( char operator) {
		switch ( operator ) {
		case '(':
		    return 0;
		case ')':
		    return -1;
		case '^':
		    return 3;        
		case '*':
			return 2;
		case '/':
			return 2;
		case '+':
			return 1;
		case '-':
			return 1;
		}
		return -1;
	}
	
	public static boolean isOperator( char c) {
	    if ( c == '+' || c == '-' || c == '/' || c == '*' || c == '^' || c == '(' || c == ')' ) {
	        return true;
	    }
	    return false;
	}
	
	public static String inFixToPreFixExpression ( String in ) throws Exception {
	    MyStackImpl stack = new MyStackImpl();
	    StringBuffer sb = new StringBuffer();
	    
	    // iterate through the inbound expression
	    for ( int i = 0; i < in.length(); i++) {
	        char ch = in.charAt(i);
	        // check if the char is an operator
            if ( isOperator( ch ) ){
                if ( ch == '(') {
                    stack.push(ch);
                }
                // if current operator precedence is gt stack top -- push on the stack
                else if ( stack.isEmpty() || (precedence(ch) > precedence(stack.top())) ) {
                    stack.push(ch);
                    sb.append(stack.pop());
                } else {
                    // if current operator precedence <= stack top -- pop
                	if (precedence(ch) <= precedence(stack.top())){
                		stack.push(ch);
                	}
                	
                    
                    
                    // ensure closing brace is not pushed on the stack
                    if ( ch != ')')
                        stack.push(ch);
                }
            }
            else {
                // non operators are sent to post fix operator
                sb.append(ch);
            }
        }
        
        // once parsing is complete - rest of the stack should be popped and appended to the postfix string
        while (! stack.isEmpty()) {
            sb.append( stack.pop() );
        }        
        return sb.toString();
	}
	
	public static String reverseString ( String in ) {
		StringBuffer sb = new StringBuffer();
		for ( int i = in.length() -1; i >=0 ; i--) {
			final char ch = in.charAt(i);
			if ( ch == '(') {
				sb = sb.append(')');
			}
			else if (ch == ')'){
				sb = sb.append('(');
			}
			else {
				sb = sb.append(in.charAt(i));
			}
		}
		return sb.toString();
	}
	
	public static void main(String[] args) {
	    String s1 = "a+b-c";
	    String s2 = "a+b*c";
	    String s3 = "a+b*c*d*e";
	    String s4 = "a+b*c^d";
	    String s5 = "a*b^c+d";
	    String s6 = "a+(b*c-d)-(e*f)";
	    String s7 = "(a*b-c)+(d*e-f)*(g+h-k)";
	    try {
	        System.out.println("Infix operator " + s1 + " converted to preFix is: " + reverseString(inFixToPreFixExpression(reverseString(s1))));
	        System.out.println("Infix operator " + s2 + " converted to preFix is: " + reverseString(inFixToPreFixExpression(reverseString(s2))));
		    System.out.println("Infix operator " + s3 + " converted to preFix is: " + reverseString(inFixToPreFixExpression(reverseString(s3))));
		    System.out.println("Infix operator " + s4 + " converted to preFix is: " + reverseString(inFixToPreFixExpression(reverseString(s4))));
		    System.out.println("Infix operator " + s5 + " converted to preFix is: " + reverseString(inFixToPreFixExpression(reverseString(s5))));
		    System.out.println("Infix operator " + s6 + " converted to preFix is: " + reverseString(inFixToPreFixExpression(reverseString(s6))));
		    System.out.println("Infix operator " + s7 + " converted to preFix is: " + reverseString(inFixToPreFixExpression(reverseString(s7))));

	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
}