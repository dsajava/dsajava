package stacks.applications;

import java.util.Stack;

public class PostfixOperator {
	
	private Stack<Integer> operandStack = new Stack<Integer>();
	
	private int compute( int op1, int op2, char operator) {
		switch ( operator ) {
		case '*':
			return op1 * op2;
		case '/':
			return op1 / op2;
		case '+':
			return op1 + op2;
		case '-':
			return op1 - op2;
		}
		return -1;
	}
	
	public int parsePostfixStringExpression (String in){
		boolean isNumber = false;
		for ( int i=0; i < in.length(); i++) {
			int tmp = 0;
			try {
				tmp = Integer.parseInt(in.charAt(i) + "");
				isNumber = true;
			}
			catch ( NumberFormatException e ){
				isNumber = false;
			}

			if ( isNumber ) {
				operandStack.push(tmp);
			}
			else {
				int operand2 = operandStack.pop();
				int operand1 = operandStack.pop();
				int operand3 = compute (operand1, operand2, in.charAt(i)); // for debug reasons
				operandStack.push( operand3 );
			}
		}
		return operandStack.pop();
	}
	
	public static void main(String[] args) {
		//String s = "345*+";
		
		PostfixOperator pf = new PostfixOperator();
		//System.out.println( "The evaluation of the PostFix expression " + s + " is " + pf.parsePostfixStringExpression(s));
		
		String s1 = "23*456/*-";
		System.out.println( "The evaluation of the PostFix expression " + s1 + " is " + pf.parsePostfixStringExpression(s1));

	}

}
