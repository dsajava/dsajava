package stacks;

public class EmptyStackException extends RuntimeException {
	private static final long serialVersionUID = 1897654321L;

	public EmptyStackException( String err ) {
		super(err);
	}
}
