package stacks;

import java.util.Scanner;

public class StackArrayImpl {
    private int top = -1;
    private static int CAPACITY = 10;
    private int [] array;
    
    StackArrayImpl() {
        this(CAPACITY);        
    }
    
    StackArrayImpl( int capacity ) {
        array = new int[capacity];
    }
    
    public void push( int item ) throws Exception {
        if ( top + 1 == array.length ) {
            throw new Exception("Stack is full");
        }
        
        array[++top] = item;
    }
    
    public int pop() throws Exception {
        if ( top == -1 ) {
           throw new Exception("Stack is empty"); 
        }
        
        int tmp = array[top];
        top--;
        return tmp;
    }
    
    public static void main( String[] args ) {
        StackArrayImpl stack = new StackArrayImpl(100);
        
        Scanner sc = new Scanner(System.in);
        try {

        while ( true ) {
            System.out.println("Enter operation to perform: \n1. push \n2. pop \n 3. stop");
            int in = sc.nextInt();
            switch ( in ) {
                case 1:
                    Scanner sc1 = new Scanner(System.in);
                    System.out.println("What value do you want to push?");
                    stack.push(sc1.nextInt());
                    break;
                case 2:
                    System.out.println("Top of the stack has:" + stack.pop());
                    break;
                case 3:
                    System.out.println("Good bye");
                    System.exit(0);
                default:
                    break;
            }
        }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        
    }
}