package stacks;

public class StackArrayForBinaryConv {
    private int top = -1;
    private static int CAPACITY = 10;
    private int [] array;
    
    StackArrayForBinaryConv() {
        this(CAPACITY);        
    }
    
    StackArrayForBinaryConv( int capacity ) {
        array = new int[capacity];
    }
    
    public void push( int item ) throws Exception {
        if ( top + 1 == array.length ) {
            throw new Exception("Stack is full");
        }
        
        array[++top] = item;
    }
    
    public int pop() throws Exception {
        if ( top == -1 ) {
           throw new Exception("Stack is empty"); 
        }
        
        int tmp = array[top];
        top--;
        return tmp;
    }
    
    boolean isEmpty() {
        return top == -1;
    }
    
    public int  convert2Binary( StackArrayForBinaryConv stack, int num ) throws Exception {
        StringBuffer sb = new StringBuffer();
        
        if ( num <= 0) return 0;
        while (num != 1 ) {
            stack.push(num%2);
            num = num / 2;
        }
        stack.push(1);
        
        while( ! stack.isEmpty() ){
            sb.append(stack.pop());
        }
        return Integer.parseInt(sb.toString());
    }   
    
    public static void main( String[] args ) {
        StackArrayForBinaryConv stack = new StackArrayForBinaryConv(100);
        try {
            System.out.println("Binary representation of 6 is :" + stack.convert2Binary(stack, 6));
            System.out.println("Binary representation of 2 is :" + stack.convert2Binary(stack, 2));
            System.out.println("Binary representation of 1 is :" + stack.convert2Binary(stack, 1));
            System.out.println("Binary representation of 10 is :" + stack.convert2Binary(stack, 10));



        }
        catch( Exception e) {
            e.printStackTrace();
        }
        
    }
}