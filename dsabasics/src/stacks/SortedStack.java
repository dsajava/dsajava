package stacks;

public class SortedStack {

    private int top = -1;
    private static int CAPACITY = 10;
    private int [] array;
    
    SortedStack() {
        this(CAPACITY);        
    }
    
    SortedStack( int capacity ) {
        array = new int[capacity];
    }
    
    public void push( int item ) throws Exception {
        if ( top + 1 == array.length ) {
            throw new Exception("Stack is full");
        }
        
        array[++top] = item;
    }
    
    public int pop() throws Exception {
        if ( top == -1 ) {
           throw new Exception("Stack is empty"); 
        }
        
        int tmp = array[top];
        top--;
        return tmp;
    }
    
    public int top() {
        return array[top];    
    }   
    
    
    public boolean isEmpty() {
        return top < 0;
    }

    public static SortedStack sort( SortedStack stack1, SortedStack stack2) throws Exception {
    
        while (! stack1.isEmpty() ){
            int tmp = stack1.pop(); // pop the top element from the first stack into a tmp var
            
            while (! stack2.isEmpty() && tmp > stack2.top() ) { // check if this tmp is gt top of the stack2
                int t = stack2.pop(); // if tmp is gt top of the sorted stack, then pop the stack and push it back to the stack 1
                System.out.println("Pushing t onto Stack1 : " + t);
                stack1.push(t);
            }
            
            System.out.println("Pushing tmp on stack2" + tmp);
            stack2.push(tmp); // push the tmp onto the sorted stack 2
        }
        return stack2;  // return stack 2 or the sorted stack
    }
    
    public static void main( String[] args ) {
        SortedStack stack1 = new SortedStack();
        SortedStack stack2 = new SortedStack();
        
        try{
            stack1.push(2);
            stack1.push(6);
            stack1.push(30);
            stack1.push(10);
            stack1.push(20);
            stack1.push(4);
        
            stack1 = sort(stack1, stack2);
        
            while (! stack1.isEmpty() ) {
                System.out.println("Elements in the stack are:" + stack1.pop());
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        
    }
}