package stacks;

class Node {
	private int _data;
	private Node _next;
	
	Node(int data) {
		_data = data;
		_next = null;
	}
	
	public Node getNext() {
		return _next;
	}
	
	public void setNext( Node node ) {
		_next = node;
	}
	
	public int getData() {
		return _data;
	}
	
	public void setData( int data ) {
		_data = data;
	}
}

public class StackUsingLinkedList {
	private Node head;
	
	private void insert( int data, Node current) {
		Node node = new Node( data );
		node.setNext(current);
		head = node;
		System.out.println("Inserting data " + data + " new head:" + head.getData());
	}

	public void push( int data ) {
        Node node = new Node(data);
        if ( head == null ) {
            head = node;
            return;
        }
        
        System.out.println("Inserting data " + data + " current head:" + head.getData());
        insert(data, head);         
    }
    
    public int pop() {
        if ( head != null ) {
            Node tmp = head;
            head = head.getNext();
            return tmp.getData();            
        }
        
        System.out.println("Stack is empty!");
        return -1;
    }
    
    public int size() {
        Node current = head;
        int size = 0;
        while (current != null){
            size++;
            current = current.getNext();
        }
        return size;
    }

    public static void main(String[] args) {
        StackUsingLinkedList stack = new StackUsingLinkedList();
        stack.push(10);
        stack.push(20);
        System.out.println("Size of stack is:" + stack.size());
        System.out.println("Stack popped:" + stack.pop());
        System.out.println("Stack popped:" + stack.pop());
        System.out.println("Stack popped:" + stack.pop());
    } 
}