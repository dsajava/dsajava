package sorting;

public class QuickSort {
    
	private static void swap(int[] array, int l, int h) {
		int temp = array[h];
		array[h] = array[l];
		array[l] = temp;
	}
	
    public static int partition(int[] array, int low, int high) {
        int pivot = array[low];
        int i = low;
        int j = high + 1;

        while( i < j ) {
        	do {
        		i++;
        	} while( array[i] < pivot && i < high);
        	
        	do {
        		j--;
        	} while (array[j] > pivot);
        	
        	if ( i < j) {
        		swap(array, i, j);
        	}
        }
        
        array[low] = array[j];
        array[j] = pivot;
        return j;
    }
    
    private static void printArray(int[] arr ) {
        for ( int i =0; i < arr.length; i++ ) {
        	System.out.print(" " + arr[i]);
        }
        System.out.println();
    }
    
    public static void quickSort(int[] array, int low, int high) {
    	if ( low < high ) {
        	int pivot = partition(array, low, high);
        	quickSort(array, low, pivot - 1);
        	quickSort(array, pivot + 1, high); 
    	}
    }
    
    public static void main(String[] args) {
        // int[] arr = { 40, 20, 10, 25, 35, 60, 67, 23, 89, 100, 77, 99, 34};
        int[] arr = { 9,5,1,8,7};
        quickSort(arr, 0, arr.length -1 );
        printArray(arr);
    }
}