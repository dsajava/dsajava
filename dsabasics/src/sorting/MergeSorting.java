package sorting;

class MergeSorting {

    public static int[] merge( int[] arr1, int[] arr2){
        int i=0, j=0, k = 0;
        
        int[] arr3 = new int[arr1.length + arr2.length];
        
        while (i < arr1.length && j < arr2.length) {
            arr3[k++] = arr1[i] <= arr2[j] ? arr1[i++]: arr2[j++];  
        }
        
        while ( i < arr1.length) {
            arr3[k++] = arr1[i++];
        }
        
        while (j < arr2.length) {
            arr3[k++] = arr2[j++];
        }

        return arr3;
    }
    
    public static void merge(int[] array, int low, int mid, int high) {
        int temp[] = new int [high-low+1];
        int l = low;
        int r = mid + 1;
        int k = 0;
        
        while(l<=mid && r <=high) {
            temp[k++] = array[l] <= array[r] ? array[l++]: array[r++];  
        }
        
        while ( l <= mid) {
            temp[k++] = array[l++];
        }
        
        while (r <= high) {
            temp[k++] = array[r++];
        }
        
        System.out.println("Temp start");
        for (int i=0; i < temp.length; i++) {
            System.out.print(" "  + temp[i]);
        }
        System.out.println("Temp stop");
        
        for ( int i = low, j =0; i<= high; i++, j++) {
            array[i] = temp[j];            
        }

    }

    public static void mergeSort(int[] array, int low, int high) {
        if ( low < high) {
        	int mid = (low + high) / 2;
	    	mergeSort( array, low, mid);
		    mergeSort( array, mid + 1, high);
		    merge ( array, low, mid, high);
        }
    }
    
    public static void main(String[] args) {
        int[] arr1 = {2,4,5,7,9};
        int[] arr2 = {3,6,11,12};
        
        int[] arr3 = merge(arr1, arr2);
        for (int i =0; i< arr3.length; i++) {
            System.out.print(" " + arr3[i]);
        }
        
        System.out.println();
        int[] array = {3,2,6,4,8,9};
        
        mergeSort(array,0, array.length -1);
        for (int i =0; i< array.length; i++) {
            System.out.print(" " + array[i]);
        }
               
    }    
}