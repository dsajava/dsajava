package sorting;

public class SelectionSort {

	/*
	 * This problem of selection sort is achieved via the same array
	 * we did not use a temp array to store the sorted elements
	 */
	
	
	/*
     * Selection sort divides the input into two parts: 
     * sorted sub-array and unsorted sub-array
     * Imagine example of cards - left hand has unsorted cards
     * while right hand will have sorted cards
     * 
     * We then repeatedly put the min element to the right hand (sorted sub-array)
     * we place it to the end of the sorted sub-array
     */ 
	
    public static void selectionSort( int[] array ) {
        int pos = -1;
        int temp = 0;
        boolean swap = false;
 
        // puts the smallest element to its right place
        for (int j=0; j < array.length - 1; j++) { 
        	// j is the index of the left sub-array where the 
        	// next min from right sub-array will be put
            pos = j;  
            swap = false; // efficient sorting
            // this loop finds the position of the next min from the right sub-array
            for ( int i = j+1; i < array.length; i++) { 
                if ( array[i] < array[pos] ) {
                    pos = i;
                    swap = true;
                }
            }
            
            // swap j with the new min from right sub-array
            if ( swap ) {
            	temp = array[pos];
            	array[pos] = array[j];
            	array[j] = temp; 
            }
        }        
    }
 
    public static void printSortedArray( int[] data ) {
        for (int k =0; k < data.length; k++ ) {
            System.out.print(" " + data[k]);
        }
    }
 
    public static void main(String[] args) {
        int[] input = {14, 10, 9, 11, 7, 8};
        selectionSort( input );
        printSortedArray(input);
    }

}
