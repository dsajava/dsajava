package sorting;

public class HeapSort {
    int index = -1;
    final static int CAPACITY = 15;
    int[] array;
    int capacity = CAPACITY;
    
    HeapSort() {
        this(CAPACITY);    
    }
    
    HeapSort (int capacity) {
        this.capacity = capacity;
        array = new int[capacity];
    }
    
    HeapSort (int[] arr) {
        this(arr.length);
        System.arraycopy(arr, 0, array, 0, arr.length);
        index = array.length - 1;
    }
    
    private int parent (int i) {
    	return (i - 1)/2;
    }
    
    private int leftChild (int i) {
    	return 2*i + 1;
    }
    
    private int rightChild (int i) {
    	return 2*i + 2;
    }
    
    public boolean isEmpty() {
    	return index == -1;
    }
    
    private void heapifyDown(int i, int cap) {
        int largest = i;
        while (true) {
        	// get left and right childs
    	    int left = leftChild(largest);
    	    int right = rightChild(largest);
    	
        	// compare A[i] with its left and right
        	// and find the largest value
    	    if ( left <= cap && array[left] > array[i] ){
    		    largest = left;
    	    }
    	
    	    if ( right <= cap && array[right] > array[largest]) {
    		    largest = right;
    	    }
    	
    	    if ( largest != i ) {
    		    int tmp = array[i];
    		    array[i] = array[largest];
    		    array[largest] = tmp;
    		    i = largest;
        	}
        	else {
        	    break;
        	}
        }
    }

    public void printAll() {
        for ( int i=0; i <= index; i++ ) {
            System.out.print( array[i] + ", ");
        }
        System.out.println();
    }
        
    public void heapify(int[] inArr, int pos) {
        for ( int i = pos; i > -1; i-- ){
            heapifyDown(i, capacity);
        }
        
        for ( int i = index; i > -1; i--){
            int tmp = inArr[0];
            inArr[0] = inArr[i];
            inArr[i] = tmp;
            index--;
            heapifyDown(0, index);
        }
    	
    }
    
    public void setIndex(int i) {
        index = i;
    }

    public static void main(String[] args) {
        int[] arr = { 40, 20, 10, 25, 35, 60, 67, 23, 89, 100, 77, 99, 34};
        HeapSort hSort = new HeapSort();
        hSort.printAll();
        hSort.heapify(arr, hSort.parent(arr.length - 1));
        hSort.setIndex(arr.length - 1);
        hSort.printAll();
    }
}