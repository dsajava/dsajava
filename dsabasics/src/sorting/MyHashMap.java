package sorting;

public class MyHashMap {
    static int[][] mapStore = new int[11][1];
    public static int DEFAULT = Integer.MAX_VALUE;
    
    public static void initialize() {
        for (int i=0; i < mapStore.length; i++) {
            for( int j=0; j < mapStore[i].length; j++) {
                mapStore[i][j] = DEFAULT;
            }
        }
    }
    
    static{
        initialize();
    }
    
    public static int hashit (int key) {
        return (((key%11)+ 1)%11);
    }
    
    public void put(int key, int value) {
        int hash = hashit(key);
        mapStore[hash][0] = value; // override the value if it already exists
    }

    public void printAll() {
        for (int i = 0; i < mapStore.length; i++) {
            for (int j=0; j < mapStore[i].length;j++) {
                if (mapStore[i][j] != Integer.MAX_VALUE) {
                    System.out.println("Hash Key: " + i + " Value: " + mapStore[i][j]);
                }
            }
        }
    } 
    
    public int search(int key) {
        int hash = hashit(key);
        return mapStore[hash][0]; // assumption - one key has one value
    }
    
    
    public static void main(String[] args) {
        MyHashMap hashMap = new MyHashMap();
        hashMap.put(23, 45);
        hashMap.put(16, 55);
        hashMap.put(54, 78);
        hashMap.printAll();
        if ( hashMap.search(23) != Integer.MAX_VALUE ) {
            System.out.println("Search result for 23 is:" + hashMap.search(23));
        }
        
        if ( hashMap.search(53) != Integer.MAX_VALUE ) {
            System.out.println("Search result for 23 is:" + hashMap.search(23));
        }

    }
}