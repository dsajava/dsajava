package sorting;

public class InsertionSort {
	
	public static void sort(int[] arr) {
		for (int i=0; i < arr.length; i++) {
			for (int j=i; j > 0; j--) {
				if (arr[j] < arr[j-1]) {
					int tmp = arr[j-1];
					arr[j-1] = arr[j];
					arr[j] = tmp;
				}
			}
		}
	}
	
	public static void main(String[] args) {
		int[] array = {5, 6, 7, 2, 9, 3};
		sort(array);
		for ( int i=0; i < array.length; i++) {
			System.out.print(array[i] + ", ");
		}
	}
}
