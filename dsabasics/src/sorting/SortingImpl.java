package sorting;

class SortingImpl {
    
	/*
	 * Bubble sort works by repeatedly sorted adjacent elements
	 * of an array if they are in incorrect order. 
	 * 
	 */
    public static void bubbleSort(int[] array) {
        int temp = 0;
        boolean swapped = false;
        for ( int j = 0; j < array.length -1; j++) { // iterate n - 1 times
            swapped = false;
            for ( int i = 0; i < array.length - 1 - j; i++) { // compare & swap adjacent elements
                if ( array[i] > array[i+1]) {
                    temp = array[i];
                    array[i] = array[i+1];
                    array[i+1] = temp;
                    swapped = true; 
                }
            }
            
            if ( ! swapped ) {  
                System.out.println("Breaking after iteration :" + j);
                break;
            }
        }
        
        printSortedArray(array);
    }
    
    /*
     * Selection sort divides the input into two parts: 
     * sorted sub-array and unsorted sub-array
     * It then repeatedly puts the min element to 
     * the end of the sorted sub-array
     */ 
    public static void selectionSort( int[] array ) {
        int pos = -1;
        int temp = 0;
        boolean swap = false;
        // puts the smallest element to its right place
        for (int j=0; j < array.length - 1; j++) { 
        	// j is the index of the left sub-array where the 
        	// next min from right sub-array will be put
            pos = j;  
            swap = false; // efficient sorting
            // this loop finds the position of the next min from the right sub-array
            for ( int i = j+1; i < array.length; i++) { 
                if ( array[i] < array[pos] ) {
                    pos = i;
                    swap = true;
                }
            }
            
            // swap j with the new min from right sub-array
            if ( swap ) {
            	temp = array[pos];
            	array[pos] = array[j];
            	array[j] = temp; 
            }
        }
        
        printSortedArray(array);
    }
    

    public static void insertionSort(int[] data) {
        for (int j=1; j < data.length; j++) {
        	int val = data[j]; // equivalent of card value at hand
        	int i = j -1; // index of the left sorted sub-array
        	
            for ( ; i >= 0; i-- ) {
            	// insertion is required, if the highest element in 
            	// left sub-array is greater than the card value in hand
                if ( data[i] > val) {  
                	// shifts data[i] to data[i+1]
                	// so a 14 10 9 8 will become 14 14 9 8
                    data [i+1] = data [i];
                }  else {
                    break;
                }              
            }
            // inserts the card in hand to its required position
            data[i + 1] = val;  
        }
        
        printSortedArray(data);
    }
    
    public static void printSortedArray( int[] data ) {
        for (int k =0; k < data.length; k++ ) {
            System.out.print(" " + data[k]);
        }
    }
       
    public static void main(String[] args) {
        int[] input = new int[] {14, 10, 9, 11, 7, 8};
        int[] selectionSortData = {14, 10, 9, 11, 7, 8};
        int[] insertSortData = {14, 10, 9, 11, 7, 8};
        
        System.out.println("Starting bubble sort");
        bubbleSort( input );
        
        System.out.println("\nStarting selection sort");
        selectionSort( selectionSortData );
        
        System.out.println("\nInsertion Sort started...");
        insertionSort(insertSortData);
        
    }
    
}