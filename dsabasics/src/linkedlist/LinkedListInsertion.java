package linkedlist;

class LinkNode {
	private int _data;
	private LinkNode _next;
	
	LinkNode(int data) {
		_data = data;
		_next = null;
	}
	
	public LinkNode getNext() {
		return _next;
	}
	
	public void setNext( LinkNode node ) {
		_next = node;
	}
	
	public int getData() {
		return _data;
	}
	
	public void setData( int data ) {
		_data = data;
	}
}

public class LinkedListInsertion {
	private LinkNode head;
	
	private void insertInFront( int data, LinkNode current) {
		LinkNode node = new LinkNode( data );
		node.setNext(current);
		head = node;
	}
	
	private void insertAt( int data, LinkNode prev, LinkNode next) {
		LinkNode node = new LinkNode( data );
		prev.setNext(node);
		node.setNext(next);
	}

	public void insertLast ( int data, LinkNode current ) {
		LinkNode node = new LinkNode( data );
		current.setNext(node);
	}

	public void insertSorted ( int data ) {
		if ( head == null ) {
			head = new LinkNode(data);
			return;
		}

		LinkNode current = head;
		if ( data <= current.getData() ) {
			insertInFront (data, current);
			return;
		}
		
		LinkNode prev = head;
		while ( current != null ) {
			if ( data <= current.getData() ) {
				insertAt (data, prev, current);
				return;
			}
			else {
				prev = current;
				current = current.getNext();
				continue;
			}
		}
		
		insertLast(data, prev);
	}
	
	public int nodeCount( LinkNode current ) {
	    
		if ( current == null ) {
			return 0;
		}
		else {
			return 1 + nodeCount ( current.getNext() );
		}
	}
	
	
	public void reverseLL() {
		LinkNode curr = head;
		LinkNode prev = null;
		LinkNode next = null;
	    
	    while ( curr != null ) {
	        next = curr.getNext();
	        curr.setNext(prev);
	        prev = curr;
	        curr = next;
	    }
	    
	    head = prev;
	}
	
	public void findMiddle() {
		LinkNode fast = head;
		LinkNode slow = head;
	    
	    if ( head == null ) {
	        return;
	    }
	    
	    while ( fast != null && fast.getNext() != null ){
	        slow = slow.getNext();
	        fast = fast.getNext().getNext();
	    }
	    
	    System.out.println("The middle element is : " + slow.getData());
	}


	public static void main(String[] args) {
		LinkedListInsertion ll = new LinkedListInsertion();
		ll.insertSorted(10);
		ll.insertSorted(10);
		ll.insertSorted(20);
		ll.insertSorted(5);
		ll.insertSorted(15);
		
		LinkNode current = ll.head;
		while ( current != null ) {
			System.out.print(current.getData() + ", ");
			current = current.getNext();
		}
		
		System.out.println();
		System.out.println("The node count in the list is: " + ll.nodeCount(ll.head));
        
        ll.reverseLL();
        current = ll.head;
        System.out.println("Reversed linked list contents are: ");
		while ( current != null ) {
			System.out.print(current.getData() + ", ");
			current = current.getNext();
		}
		
		ll.findMiddle();

	}
}
