package linkedlist;

class LLNode {
    private int data;
    private LLNode next;
    
    LLNode(int data){
        this.data = data;
        next = null;
    }
    
    public LLNode getNext() {
        return next;
    }
    
    public void setNext(LLNode node) {
        next = node;
    }
    
    public int getData() {
        return data;
    }
    
    public void setData( int data_) {
        data = data_;
    }
}

public class LL {
    LLNode head;
    
    public void insert(int data){
        LLNode node = new LLNode(data);
        if ( head == null ) {
            head = node;
            return;
        }
         
        LLNode last = head;
        while ( last.getNext() != null ) {
            last = last.getNext();
        }
        
        last.setNext(node);
    }
    
    public String toString() {
        StringBuffer sb = new StringBuffer();
        LLNode last = this.head;
        
        while (last != null) {
            sb.append(last.getData());
            sb.append(", ");
            last = last.getNext();
            
        }

        return sb.toString();
    }
    
    public void delete ( int data ) {
        LLNode current = this.head;
        LLNode next = current.getNext();
        
        // Scenario 1 - if head is null
        if ( current == null ) {
            return;
        }
        
        if ( current.getData() == data ) {
            head = current.getNext();
            return;
        }
        
        // Iterate over the list to find the value to delete
        while ( next != null ) {
            if ( next.getData() == data ) {
                current.setNext(next.getNext());
                System.out.println("Data found and deleted");
                return;
            }
            current = current.getNext();
            next = current.getNext();
        }
        System.out.println("No data found with " + data);
    }
    
    public static void main(String[] args) {
        LL ll = new LL();
        ll.insert(300);
        ll.insert(400);
        ll.insert(500);
        ll.insert(900);
        System.out.println("Contents of the LL are:" + ll.toString());
        ll.delete(500);
        ll.delete(900);
        ll.delete(300);
        ll.delete(780);
        ll.delete(400);
        System.out.println("Contents of the LL are:" + ll.toString());

    }
}