package linkedlist;

class CLNode {
    private int data;
    private CLNode next;
    
    CLNode(int data) {
        this.data = data;
        next = this;
    }
    
    public int getData() {
        return data;
    }
    
    public CLNode getNext() {
        return next;
    }
    
    public void setData(int data) {
        this.data = data;
    }
    
    public void setNext( CLNode node ) {
        next = node;
    }
}

public class CircularLL {
    CLNode head;
    
    public void insert( int data ) {
       CLNode node = new CLNode(data);
       CLNode curr = head;
       
       if ( head == null ) {
           head = node;
           return;
       }
       
       if ( head.getNext() != head ) {
            CLNode last = head.getNext();
            head.setNext(node);
            node.setNext(last);
            head = node;
            return;
       }
       else {
            node.setNext(head);
            curr.setNext(node);
            head = node;
            return;
        }       
    }

    public void delete( int data ) {
       CLNode curr = head;
       if ( head == null ) {
           System.out.println("No such node");
           return;
       }
       
       // if the current node is head
       if ( head.getData() == data ) {
           if ( curr.getNext() != head ) {
                CLNode next = curr.getNext();
                while ( curr.getNext() != head ) {
                    curr = curr.getNext();
                }
                curr.setNext(next);
                head = next;
                System.out.println("Node found and deleted");
                printCLL();
                return;
           } else {
                head = null;
                System.out.println("Node found and deleted");
                return;
           }
       }
       
       // for non head node
       while ( curr.getNext().getData() != data ) {
           curr = curr.getNext();
       }
       
       CLNode nextNode = curr.getNext().getNext();
       curr.setNext(nextNode);
       System.out.println("Node found and deleted");
       printCLL();
       return;
    }
    
    public void printCLL() {
        CLNode curr = head;
        if ( head == null ) {
            return;
        }
        
        System.out.print("CLL Printed here :");
        while ( curr.getNext() != head ) {
            System.out.print(curr.getData() + ",");
            curr = curr.getNext();
        }
        System.out.print(curr.getData());
        System.out.println();
    }
    
    public static void main(String[] args ) {
        CircularLL cl = new CircularLL();
        
        cl.insert(10);
        cl.insert(20);
        cl.insert(45);
        cl.insert(15);
        cl.insert(90);
        cl.printCLL();
        cl.delete(90);
        cl.insert(100);
        cl.insert(200);
        cl.printCLL();
        cl.delete(45);
    }
    
}