package linkedlist;

public class BigIntegerNumbers {
    private LLNode head;
    
    public LLNode getHead() {
        return head;
    }

	public void insert(int data){
        LLNode node = new LLNode(data);
        if ( head == null ) {
            head = node;
            return;
        }
         
        LLNode last = head;
        while ( last.getNext() != null ) {
            last = last.getNext();
        }
        
        last.setNext(node);
    }
    
    public void printLL () {
        LLNode current = head;
        System.out.println("Linked list data contents are : ");
		while ( current != null ) {
			System.out.print(current.getData() + " ");
			current = current.getNext();
		}
		System.out.println();
    }
    
    public void insertString (String num) {
        for ( int i=0; i < num.length(); i++){
            insert(Character.getNumericValue(num.charAt(i)));            
        }        
    }
    
    public BigIntegerNumbers addLists(LLNode inX) {
        BigIntegerNumbers result = new BigIntegerNumbers();
        int sum = 0;
        int carry = 0;
        int x = 0;
        int y = 0;
        LLNode curr = head;
        
        while ( inX != null || curr != null ) {
            if ( inX == null) {
                x = 0;                
            } else {
                x = inX.getData();
            }
            
            if ( curr == null ) {
                y = 0;
            } else {
                y = curr.getData();
            }
            
            sum = carry + x + y;
            
            if ( sum >= 10 ) {
                carry = sum /10;
                sum = sum % 10;
            }
            else {
                carry = 0;    
            }
            result.insert(sum);
            
            if ( inX != null )
                inX = inX.getNext();
                
            if ( curr != null )
                curr = curr.getNext();            
        }
        return result;
    } 
    
    public void printReverseLL ( LLNode node) {
        if ( node == null ) {
            return;
        }
        
        printReverseLL ( node.getNext());
        System.out.print( node.getData() + " ");
    }
    
    public static String reverseString(String in) {
        StringBuilder num = new StringBuilder();
        num.append(in);
        num = num.reverse();
        return num.toString();
    }

    public static void main(String[] args) {
        String number1 = "1236";
        String number2 = "234";
        
        System.out.println("String is " + reverseString(number1));
        
        BigIntegerNumbers big1 = new BigIntegerNumbers();
        big1.insertString (reverseString(number1));
        System.out.println("Linked list contents are:");
        big1.printReverseLL(big1.getHead());
        System.out.println("");

        
        BigIntegerNumbers big2 = new BigIntegerNumbers();
        big2.insertString (reverseString(number2));
        System.out.println("Linked list contents are:");
        big2.printReverseLL(big2.getHead());
        System.out.println();
        
        BigIntegerNumbers bigResult = big1.addLists( big2.getHead());
        System.out.println("Linked list contents are:");
        bigResult.printReverseLL(bigResult.getHead());
    }
}