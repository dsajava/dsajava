package heaps;

public class MaxHeap {
    int index = -1;
    final static int CAPACITY = 15;
    int[] array;
    int capacity = CAPACITY;
    
    MaxHeap() {
        this(CAPACITY);    
    }
    
    MaxHeap(int capacity) {
        this.capacity = capacity;
        array = new int[capacity];
    }
    
    private int parent (int i) {
    	return (i - 1)/2;
    }
    
    private int leftChild (int i) {
    	return 2*i + 1;
    }
    
    private int rightChild (int i) {
    	return 2*i + 2;
    }
    
    
    /*
     * The new element is initially appended to the end of the 
     * heap (as the last element of the array). The heap property is 
     * repaired by comparing the added element with its parent and 
     * moving the added element up a level (swapping positions with 
     * the parent). This process is called "percolation up". 
     * The comparison is repeated until the parent is 
     * larger than or equal to the percolating element.
     */
    public void enqueue( int data ) {
    	if ( index == capacity ) {
    		System.out.println("Heap full error");
    		return;
    	}
    	
        array[++index] = data;
        
        // bubble up
        for(int i = index; i > 0; i = parent(i)) {
            int j = (i - 1)/2;
            if ( data > array[j]) {
                int tmp = array[j];
                array[j] = data;
                array[i] = tmp;
            }	
            else {
                break;
            }
        }
    }
    
    public boolean isEmpty() {
    	return index == -1;
    }
    
    private void heapifyDown(int i) {
        int largest = i;
        while (true) {
        	// get left and right childs
    	    int left = leftChild(largest);
    	    int right = rightChild(largest);
    	
        	// compare A[i] with its left and right
        	// and find the largest value
    	    if ( array[left] > array[i] ){
    		    largest = left;
    	    }
    	
    	    if ( array[right] > array[largest]) {
    		    largest = right;
    	    }
    	
    	    if ( largest != i ) {
    		    int tmp = array[i];
    		    array[i] = array[largest];
    		    array[largest] = tmp;
    		    i = largest;
        	}
        	else {
        	    break;
        	}
        }
    }
    
    public int dequeue () {
    	if ( isEmpty()) {
    		System.out.println("Heap is empty");
    		return -1;
    	}
    	
    	int value = array[0];
    	array[0] = array[index];
    	index--;
    	heapifyDown(0);
		return value;
    }
    
    public void printAll() {
        for ( int i=0; i <= index; i++ ) {
            System.out.print( array[i] + ", ");
        }
        System.out.println();
    }
    
    public static void main(String[] args) {
        MaxHeap mHeap = new MaxHeap(20);
        
        int[] arr = { 40, 20, 10, 25, 35, 60, 67, 23, 89, 100};
        for ( int i : arr) {
            mHeap.enqueue(i);            
        }
        
        mHeap.printAll();
        mHeap.dequeue();
        System.out.println("Dequeued index 2");
        mHeap.printAll();
        System.out.println("");
    }
    
}