package arrays;

public class ArraySum {

    public static int arraySum( int[] arr, int n) {
       
        if ( n == 0 ) {
            return arr[0];
        }
        else {
            return arr[n] + arraySum(arr, n-1);
        }
       
    }
    
    public static void main (String[] args) {
        int[] arr = {1,2,3,4};
        System.out.println(arraySum(arr, arr.length-1));
    }
}
