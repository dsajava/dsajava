package arrays;

public class ArraySumOfOddValues {


    public static int arraySum( int[] arr, int n) {
       
        if ( n == 0 ) {
            return arr[0]%2==0 ? 0 : arr[0];
        }
        else {
            if(arr[n]%2 != 0) {
                return arr[n] + arraySum(arr, n-1);
            } else {
                return arraySum(arr,n-1);
            }
        }
    }
    
    public static void main(String[] args) {
        int[] arr1 = {2, 4,6, 8};
        int[] arr2 = {1,2,3,4,5};
        
        System.out.println("Sum of odd values in arr1 :" + arraySum(arr1, arr1.length -1 ));
        System.out.println("Sum of odd values in arr2 :" + arraySum(arr2, arr2.length -1 ));
    }
}