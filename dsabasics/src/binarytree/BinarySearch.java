package binarytree;

public class BinarySearch {
    
    public static int binarySearchNR ( int[] arr, int key ) {
        int low = 0;
        int high = arr.length - 1;
        
        while ( low <= high ) {
            
            int mid = (low + high)/2;
            
            if ( key < arr[mid] ) {
                high = mid  - 1;
            }
            else if ( key == arr[mid] ) {
                return key;
            }
            else {
                low = mid + 1;
            }
        }
        return -1;
    }
    
    public static int binarySearch ( int[] arr, int low, int high, int key) {
        if ( low > high ) {
            return -1; // base case 
        }
        else {
            int mid = (low + high) / 2; // find mid point
            
            if ( key < arr[mid]) { // search the lower half
                return binarySearch(arr, low, mid - 1, key); // reset the high mark and search the lower half
            }
            else if ( key == arr[mid]) { // find match
                return arr[mid];
            }
            else {
                return binarySearch(arr, mid + 1, high, key); // recursively search the upper half after resetting the lower watermark
            }    
        }
    }
    
    public static void main( String[] args ) {
        int[] array = { 4, 10, 15, 20, 25};
        System.out.println( "Find 15: " + binarySearchNR( array, 15));
        System.out.println( "Find 30: " + binarySearchNR( array, 30));
        
        System.out.println( "Find 15 using recursion:" + binarySearch( array, 0, array.length -1,  15));
    }
}