package binarytree;

import java.util.Scanner;

class Node {
    int data;
    Node left, right;
    
    Node(int data) {
        this.data = data;
        left = null;
        right = null;
    }
    
    int getData() {
        return data;
    }
    
    Node getLeft() {
        return left;
    }
    
    Node getRight() {
        return right;
    }
    
    void setLeft(Node node) {
        left = node;
    }
    
    void setRight(Node node) {
        right = node;
    }
    
    void setData(int data) {
        this.data = data;
    }
}

public class BinaryTreeImpl {
    Node root;
    
    public void insert( int data ) {
        Node node = new Node(data);
        if ( root == null ) {
            root = node;
            return;
        }
        
        Scanner sc = null;
        try { 
            sc = new Scanner(System.in);
            Node current = root, previous = null;
            int side = -1;
            
            while ( current != null ) {
                System.out.print("Do you want to enter" + data + " to the left of " + current.getData() + " or to its right (enter 0 for left and 1 for right):");
                side = sc.nextInt(); 
                previous = current;
                if ( side == 0) {
                    current = current.getLeft();
                }
                else if ( side == 1) {
                    current = current.getRight();
                }
            }
            
            if(side == 0) {
                previous.setLeft(node);
            } else {
                previous.setRight(node);
            }
        }
        catch (Exception ex ) {
        	ex.getStackTrace();
        }
        finally {
            sc.close();    	
        }
        return;
    }

    public Node insertR( Node n, int data ) {
        Node node = new Node(data);
        Node current = n;
        if ( n == null ) {
            return node;
        }
        
        Scanner sc = new Scanner(System.in);
        int side = -1;
        
        System.out.print("Do you want to enter" + data + " to the left of " + current.getData() + " or to its right (enter 0 for left and 1 for right):");
        side = sc.nextInt(); 
        if ( side == 0) {
            current.left = insertR(current.getLeft(), data);
        }
        else if ( side == 1) {
            current.right = insertR(current.getRight(), data);
        }
        return current;
    }

    public void inOrderTraversal(Node node) {
        if ( node == null ) {
            return;
        }
        
        inOrderTraversal(node.getLeft());
        System.out.print(node.getData() + " ");
        inOrderTraversal(node.getRight());
    }
    
    public void preOrderTraversal(Node node) {
    	if ( node == null ) {
    		return;
    	}

    	System.out.print(node.getData() + " ");
    	preOrderTraversal(node.getLeft());
    	preOrderTraversal(node.getRight());
    }

    public void postOrderTraversal(Node node) {
    	if ( node == null ) {
    		return;
    	}

    	preOrderTraversal(node.getLeft());
    	preOrderTraversal(node.getRight());
    	System.out.print(node.getData() + " ");
    }

    public Node getRoot() {
        return root;
    }
    
    public void setRoot(Node node) {
        root = node;    
    }
    
    public int findSemiLeafCount(Node node) {
    	if ( node == null ) {
    		return 0;
    	}
    	
    	if (node.getLeft() == null && node.getRight() == null ) {
    		return 0;
    	}
    	
    	if (node.getLeft() == null || node.getRight() == null ) {
    		return 1;
    	}

    	
    	return findSemiLeafCount(node.getLeft()) + findSemiLeafCount(node.getRight()) + 1;
    }
    
    public int findLeafCount(Node node) {
    	if ( node == null ) {
    		return 0;
    	}
    	
    	if (node.getLeft() == null && node.getRight() == null ) {
    		return 1;
    	}
    	
    	return findLeafCount(node.getLeft()) + findLeafCount(node.getRight());
    }

    
    private Node find( Node node_, int data ) {
    	if ( node_ != null ) {
    		if ( node_.getData() == data ) {
    			return node_;
    		}
    
    		Node node = find(node_.getLeft(), data);
    		if ( node == null ) {
    			node = find(node_.getRight(), data);
    		}
    		return node;
    	}
    	return null;
    }
    
    public boolean find(int data) {
    	return find(root, data) != null;
    }
    
    public int findMax(Node node) {
        if (node == null ) {
            return -1;
        }
        int data = node.getData();
        int maxLeft = findMax(node.getLeft());
        int maxRight = findMax(node.getRight());
        if ( data > maxLeft ) {
            return (data > maxRight ? data : maxRight);
        }
        else {
            return (maxLeft > maxRight? maxLeft : maxRight);
        }
    }
    
    public int findHeight(Node node) {
        if ( node == null || ( node.getLeft() == null && node.getRight() == null  )) {
            return 0;
        }
        
        int leftHeight = findHeight(node.getLeft());
        int rightHeight = findHeight(node.getRight());

        return  (leftHeight > rightHeight ? leftHeight + 1 : rightHeight + 1);
    }
    
    public static void main(String[] args) {
        BinaryTreeImpl tree = new BinaryTreeImpl();
        tree.setRoot(tree.insertR(tree.getRoot(), 30));
        tree.insertR(tree.getRoot(), 18);
        tree.insertR(tree.getRoot(), 12);
        
        System.out.println("Leaf count is:" + tree.findLeafCount(tree.getRoot()));
        System.out.println("Semi Leaf count is:" + tree.findSemiLeafCount(tree.getRoot()));

        
        System.out.print("InOrderTraversal: ");
        tree.inOrderTraversal(tree.getRoot());
        System.out.println(" ");

        System.out.print("PreOrderTraversal: ");
        tree.preOrderTraversal(tree.getRoot());
        
        System.out.println(" ");
        System.out.print("PostOrderTraversal: ");

        tree.postOrderTraversal(tree.getRoot());

        System.out.println(" ");
        
        System.out.println("Find...18 " + tree.find(18));
        System.out.println("Find...20 " + tree.find(20));
        System.out.println("Find...30 " + tree.find(30));
        System.out.println("Find...12 " + tree.find(12));
        System.out.println("Find...35 " + tree.find(35));
        
        System.out.println("Max Node in tree is:" + tree.findMax(tree.getRoot()));
        System.out.println("Height of the tree is:" + tree.findHeight(tree.getRoot()));
    }
}