package binarytree;

import java.util.Scanner;

public class StrictBinaryTreeImpl {
    Node root;
    
    public void insert( int data ) {
        Node node = new Node(data);
        if ( root == null ) {
            root = node;
            return;
        }
        
        Scanner sc = new Scanner(System.in);
        Node current = root, previous = null;
        int side = -1;
        
        while ( current != null ) {
            System.out.print("Do you want to enter" + data + " to the left of " + current.getData() + " or to its right (enter 0 for left and 1 for right):");
            side = sc.nextInt(); 
            previous = current;
            if ( side == 0) {
                current = current.getLeft();
            }
            else if ( side == 1) {
                current = current.getRight();
            }
        }
        
        if(side == 0) {
            previous.setLeft(node);
        } else {
            previous.setRight(node);
        }
        return;
    }

    public Node insertR( Node n, int data ) {
        Node node = new Node(data);
        Node current = n;
        if ( n == null ) {
            return node;
        }
        
        Scanner sc = new Scanner(System.in);
        int side = -1;
        
        System.out.print("Do you want to enter" + data + " to the left of " + current.getData() + " or to its right (enter 0 for left and 1 for right):");
        side = sc.nextInt(); 
        if ( side == 0) {
            current.left = insertR(current.getLeft(), data);
        }
        else if ( side == 1) {
            current.right = insertR(current.getRight(), data);
        }
        return current;
    }

    public Node getRoot() {
        return root;
    }
    
    public void setRoot(Node node) {
        root = node;    
    }
    
    public int findSemiLeafCount(Node node) {
    	if ( node == null ) {
    		return 0;
    	}
    	
    	if (node.getLeft() == null && node.getRight() == null ) {
    		return 0;
    	}
    	
    	if (node.getLeft() == null || node.getRight() == null ) {
    	    return findSemiLeafCount(node.getLeft()) + findSemiLeafCount(node.getRight()) + 1;
    	}
    	
    	return findSemiLeafCount(node.getLeft()) + findSemiLeafCount(node.getRight());
    }
    
    public int findLeafCount(Node node) {
    	if ( node == null ) {
    		return 0;
    	}
    	
    	if (node.getLeft() == null && node.getRight() == null ) {
    		return 1;
    	}
    	
    	return findLeafCount(node.getLeft()) + findLeafCount(node.getRight());
    }
    
    public boolean checkIfBinaryTreeIsStrict( Node node ) {
        if ( node == null ) {
            return false;
        }
        
        if ( node.getRight() == null && node.getLeft() == null) {
            return true;
        }
        
        if ( node.getRight() == null || node.getLeft() == null) {
            return false;
        }
        
        return checkIfBinaryTreeIsStrict(node.getLeft()) && checkIfBinaryTreeIsStrict(node.getRight());   
    }    
    
    public int findHeight(Node node) {
        if ( node == null) {
            return 0;
        }
        
        if ( node.getLeft() == null && node.getRight() == null ) {
            return 1;
        }
        
        int leftHeight = findHeight(node.getLeft());
        int rightHeight = findHeight(node.getRight());

        return  (leftHeight > rightHeight ? leftHeight + 1 : rightHeight + 1);
    }

    public int checkIfTreeIsComplete(Node node) {
        if ( node == null ) {
            return 0;
        }
        
        int left = findHeight(node.getLeft());
        int right = findHeight(node.getRight());
        
        System.out.println("Right left values are: " + right + " " + left);
        if ( left != right || left ==0 || right == 0 ) {
            return 0;
        } else {
            return left + 1;
        }
        
    }
    
    public static void main(String[] args) {
        StrictBinaryTreeImpl tree = new StrictBinaryTreeImpl();
        tree.setRoot(tree.insertR(tree.getRoot(), 30));
        tree.insertR(tree.getRoot(), 18);
        tree.insertR(tree.getRoot(), 12);
        //tree.insertR(tree.getRoot(), 14);
        //tree.insertR(tree.getRoot(), 15);
        //tree.insertR(tree.getRoot(), 25);
        //tree.insertR(tree.getRoot(), 35);



        System.out.println("Leaf count is:" + tree.findLeafCount(tree.getRoot()));
        System.out.println("Semi Leaf count is:" + tree.findSemiLeafCount(tree.getRoot()));
        System.out.println("Tree is Strict Binary Tree: " + tree.checkIfBinaryTreeIsStrict(tree.getRoot()));
        System.out.println("Tree is Complete Binary Tree: " + tree.checkIfTreeIsComplete(tree.getRoot()));

   }
}