package binarytree;

class StackArrayImpl {
    private int top = -1;
    private static int CAPACITY = 10;
    private BSTNode [] array;
    
    StackArrayImpl() {
        this(CAPACITY);        
    }
    
    StackArrayImpl( int capacity ) {
        array = new BSTNode[capacity];
    }
    
    public void push( BSTNode item ) throws Exception {
        if ( top + 1 == array.length ) {
            throw new Exception("Stack is full");
        }
        
        array[++top] = item;
    }
    
    public BSTNode pop() throws Exception {
        if ( top == -1 ) {
           throw new Exception("Stack is empty"); 
        }
        
        BSTNode tmp = array[top];
        top--;
        return tmp;
    }
    
    public BSTNode peek() throws Exception {
        if ( top == -1 ) {
            throw new Exception("Stack is empty"); 
         }
        return array[top];
    }
    
    public boolean isEmpty() {
        return top == -1;
    }
}

class QNode {
	private BSTNode  _data;
	private QNode _next;
	
	QNode(BSTNode data) {
		_data = data;
		_next = null;
	}
	
	public QNode getNext() {
		return _next;
	}
	
	public void setNext( QNode node ) {
		_next = node;
	}
	
	public BSTNode getData() {
		return _data;
	}
	
	public void setData( BSTNode data ) {
		_data = data;
	}
}

class Queue {
    QNode rear, front;
    
	public void enqueue (BSTNode data){
        QNode node = new QNode(data);
        if ( rear == null ) {
            rear = node;
            front = node;
            return;
        }
         
        rear.setNext(node);
        rear = rear.getNext();
    }
    
    public BSTNode dequeue() {
        if ( front == null ) {
            System.out.println("Queue is empty");
            return null;
        }
        
        BSTNode tmp = front.getData();
        front = front.getNext();
        if ( front == null ) {
            rear = null;
        }
        return tmp;
    }
    
    public boolean isEmpty() {
        return rear == null && front == null;
    }
}

class BSTNode {
    int data;
    BSTNode left, right;
    
    BSTNode(int data) {
        this.data = data;
        left = null;
        right = null;
    }
    
    int getData() {
        return data;
    }
    
    BSTNode getLeft() {
        return left;
    }
    
    BSTNode getRight() {
        return right;
    }
    
    void setLeft(BSTNode node) {
        left = node;
    }
    
    void setRight(BSTNode node) {
        right = node;
    }
    
    void setData(int data) {
        this.data = data;
    }
}

public class SimpleBinarySearchTree {
    private BSTNode root;
    
    private BSTNode insert( BSTNode node, int data ) {
    	BSTNode local = new BSTNode(data);
        if ( node == null ) {
            node = local;
            return node;
        }
        
        if ( data < node.getData() ) {
            node.setLeft( insert(node.getLeft(), data));
        } else {
            node.setRight( insert(node.getRight(), data));
        }
        return node;
    }
    
    public void insert( int data ) {
        root = insert(root, data);
    }
    
    // always presents data in sorted order
    public void inOrderTraversal(BSTNode node) {
        if ( node == null ) {
            return;
        }
        
        inOrderTraversal(node.getLeft());
        System.out.print(node.getData() + " ");
        inOrderTraversal(node.getRight());
    }
    
    public BSTNode getRoot() {
    	return root;
    }
    
    public void setRoot(BSTNode root) {
        this.root = root;
    }
    
    private int findMin(BSTNode node) {
    	if ( node.getLeft() != null ) {
    		return findMin(node.getLeft());
    	}
    	return node.getData();
    }
    
    public BSTNode deleteNode(BSTNode node, int data) {
    	if ( node == null ) {
    		return null;
    	}

    	if ( data < node.getData()) {
    		node.setLeft(deleteNode(node.getLeft(), data));
    	} 
    	else if ( data > node.getData() ) {
    		node.setRight(deleteNode(node.getRight(), data));
    	}
    	else {
    		if ( node.getLeft() == null && node.getRight() == null) {
        		return null;
        	}
        	
    		if ( node.getLeft() == null ) {
        		node = node.getRight();
        	}
        	else if ( node.getRight() == null ) {
        		node = node.getLeft();
        	}
        	else {
        		int min = findMin(node.getRight());
        		node.setData(min);
        		node.setRight(deleteNode(node.getRight(), min));
                System.out.println("deleting "+data);
        	}
    	}
    	
    	return node;
    }

    public BSTNode lowestCommonAncestor(BSTNode node, int data1, int data2) {
    	if ( node == null ) {
    		return null;
    	}
    	
    	if ( node.getData() < data1 && node.getData() < data2) {
    		return lowestCommonAncestor(node.getRight(), data1, data2);
    	}
    	else if ( node.getData() > data1 && node.getData() > data2) {
    		return lowestCommonAncestor(node.getLeft(), data1, data2);
    	}

    	return node;
    }

    public void inOrderTraversal () throws Exception {
        if ( root == null ) {
            System.out.println("BST is empty");
            return;
        }
        
        StackArrayImpl stack = new StackArrayImpl();
        BSTNode current = root;
        stack.push( root );
        current = current.getLeft();
        
        while (! stack.isEmpty() || current != null ) {
            if ( current != null ) {
                stack.push( current );
                current = current.getLeft();
            }
            else {
                BSTNode top = stack.pop();
                System.out.print(top.getData() + "," );

                if (top.getRight() != null) {
                    current = top.getRight();
                }
            }
        }
    }   

    public void preOrderTraversal () throws Exception {
        if ( root == null ) {
            System.out.println("BST is empty");
            return;
        }
        
        StackArrayImpl stack = new StackArrayImpl();
        stack.push(root);
        
        while (! stack.isEmpty()) {
            BSTNode top = stack.pop();
            System.out.print(top.getData() + ", " );
            if (top.getRight() != null) {
                stack.push(top.getRight());
            }

            if (top.getLeft() != null) {
                stack.push(top.getLeft());
            }
        }
    } 
    
    public void postOrderTraversal() throws Exception {
    	if ( root == null ) {
            System.out.println("BST is empty");
            return;
    	}
    	
        StackArrayImpl stack = new StackArrayImpl();
        BSTNode current = root;
        
        while ( true ) {
        	if ( current != null ) {
        		if ( current.right != null ) {
        			stack.push(current.right);
        		}
        		stack.push(current);
        		current = current.left;
        		continue;
        	}
        	
        	if (stack.isEmpty()) {
        		return;
        	}
        	
        	current = stack.pop();
        	if ( current.right != null && ! stack.isEmpty() && current.right == stack.peek()) {
        		stack.pop();
        		stack.push(current);
        		current = current.right;
        	}
        	else {
        		System.out.print(current.data + " ");
        		current = null;
        	}
        }
    }
    
    
    public void levelOrderTraversal ( ) {
        if ( root == null ) {
        	System.out.println("BST is empty");
            return;
        }
        
        Queue queue = new Queue();
        queue.enqueue(root);

        while (! queue.isEmpty() ) {
            BSTNode node = queue.dequeue();
            System.out.print(node.getData() + ", ");
            
        	if ( node.getLeft() != null ) {
        	    queue.enqueue(node.getLeft());
        	}
        	
        	if ( node.getRight() != null ) {
        	    queue.enqueue(node.getRight());
        	}
        }        
    }

	public void setup(int[] array) {
        for ( int i : array ) {
    	    insert(i);
        }
	}
	
    public static void main(String[] args) {
    	SimpleBinarySearchTree tree = new SimpleBinarySearchTree();
    	
        int[] array = {18,14,12,10,13,25,27,26,29};
        tree.setup(array);
        
    	try {
            System.out.print("PreOrderTraversal: ");
            tree.preOrderTraversal();
            System.out.println(" ");
            
            System.out.print("Iterative InOrderTraversal: ");
            tree.inOrderTraversal();
            System.out.println(" ");
            
            System.out.println("Iterative PostOrderTraversal...");
            tree.postOrderTraversal();
    	}
    	catch (Exception e ) {
    	    e.printStackTrace();
    	}
    	
        System.out.print("Recursive InOrderTraversal: ");
        tree.inOrderTraversal(tree.getRoot());
        System.out.println(" ");
                
        
        int[] array1 = {10, 8, 6, 9, 12, 15, 14, 13};
        SimpleBinarySearchTree tree1 = new SimpleBinarySearchTree();
        tree1.setup(array1);
        
        System.out.println("Root is" + tree1.getRoot().getData());
        tree1.deleteNode(tree1.getRoot(), 9);
        tree1.deleteNode(tree1.getRoot(), 8);
        tree1.deleteNode(tree1.getRoot(), 12);
        tree1.setRoot(tree.deleteNode(tree1.getRoot(), 10));
    	
        System.out.print("InOrderTraversal: ");
        tree1.inOrderTraversal(tree.getRoot());
        System.out.println(" ");
        System.out.println("Root is" + tree1.getRoot().getData());
        
        System.out.println("Lowest common ancestor for 10 and 16 is:" + tree.lowestCommonAncestor(tree.root, 10, 16).getData());
        System.out.println("Lowest common ancestor for 15 and 25 is:" + tree.lowestCommonAncestor(tree.root, 10, 25).getData());
    }
}
