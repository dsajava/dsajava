package oldlinkedlists;

public class DListNode<T> {
	private T data;
	private DListNode<?> next;
	private DListNode<?> prev;
	
	public DListNode () {
		this(null);
	}
	
	DListNode ( T data ) {
		this.data = data;
		this.next = null;
		this.prev = null;
	}
}
