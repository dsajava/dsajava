package oldlinkedlists;

import java.util.Arrays;
import java.util.Comparator;

public class FastSListImpl<T> implements FastSList<T> {

	private static final int DEFAULT_CAPACITY  = 10;
	private final double _growthFactor;
	
	private int _next = 0;
	private T[] _array;
	private int _arrayLen;
	
	public FastSListImpl(){
		this( DEFAULT_CAPACITY );
	}
	
	@SuppressWarnings("unchecked")
	public FastSListImpl( int initialSize ){
		_array = (T[]) new Object[initialSize]; 
		_arrayLen = initialSize;
		_growthFactor = 1.5;
	}
	
	public int size() {
		return _next;
	}
	
	public int capacity() {
		return _arrayLen;
	}
	
	public void add( T o ) {
		ensureCapacity( _next + 1 );
		_array[_next++] = o;
	}
	
	public void addAll( FastSList<T> elements) {
		int requiredSize = elements.size();
		ensureCapacity( _next + requiredSize );
		for ( int i= 0; i < requiredSize; i++) {
			_array[_next++] = elements.get( i );
		}
	}
	
	public boolean remove( T o ){
		if ( o == null ) {
			return false;
		}
		
		for (int i=0; i < _arrayLen; i++){
			if ( o == _array[i] || o.equals( _array[i] )) {
				remove(i);
				return true;
			}
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	public final void ensureCapacity( int capacity ) {
		if ( capacity > _arrayLen ) {
			_arrayLen = (int) (capacity * _growthFactor);
			T[] newArray = (T[]) new Object[_arrayLen];
			copy( _array, 0, newArray, 0, _next);
			_array = newArray;
		}
	}
	
	public T get( int index ) {
		rangeCheck( index, _next );
		return _array[index];
	}
	
	public T set( int index, T element ){
		rangeCheck( index, _next );
		ensureCapacity( _next + 1 );
		T temp = _array[index];
		_array[index] = element;
		return temp;
	}
	
	private static void rangeCheck( int index, int size ) {
		if ( index >= size ) {
			throw new IndexOutOfBoundsException ( "Index: " + index + ", Size: " + size);
		}
	}
	
	private void copy( T[] src, int srcPos, T[] dest, int destPos, int length) {
		if ( destPos - srcPos == 0 && src == dest ) {
			return;
		}
		
		System.arraycopy(src, srcPos, dest, destPos, length);
	}
	
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append( '[' );
		
		for ( int i=0; i < _next; i++) {
			s.append( _array[i] );
			if ( i < _next - 1) {
				s.append( ',' );
			}
		}
		s.append( ']' );
		return s.toString();
	}
	
	public T remove(int index) {
		rangeCheck( index, _arrayLen );
		
		T element = _array[index];
		if ( index < _arrayLen - 1) {
			copy( _array, index + 1, _array, index, _arrayLen - index - 1);
		}
		_array[_arrayLen - 1] = null;
		_next--;
		return element;
	}
	
	public void clear() {
		Arrays.fill( _array, 0, _next, null);
	}
	
	public void sort( Comparator<? super T> comparator) {
		Arrays.sort(_array, 0, _next, comparator);
	}
	
	public static void main( String[] args ) {
		FastSList<Integer> l1 = new FastSListImpl<Integer>();
		l1.add(70);
		l1.add(20);
		l1.add(30);
		l1.add(10);
		l1.add(80);
		System.out.println("Linked list after add looks like: " + l1.toString());
		
		l1.sort( new MyIntComparator() );

		System.out.println("Linked list after sort looks like: " + l1.toString());

		l1.remove(30);
		System.out.println("Linked list after remove looks like: " + l1.toString());
		
		Integer var = l1.set(3, 90);
		System.out.println("Linked list after set looks like: " + l1.toString());
		System.out.println("Returned var is: " + var.intValue());

		l1.clear();
		System.out.println("Linked list after clear looks like: " + l1.toString());

		l1.remove(20);
		System.out.println("Linked list looks like: " + l1.toString());

	}
}
