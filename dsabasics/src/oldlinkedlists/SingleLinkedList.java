package oldlinkedlists;

public class SingleLinkedList {
	private SListNode head;
	private int size;
	
	public SingleLinkedList() {
		head = null;
		size = 0;
	}
	
	public int size() {
		return size;
	}
	
	public void insertFront(Object e) {
		head = new SListNode( (int)e, head );
		size++;
	}
	
	public void insertBack( Object e ) {
		SListNode node = new SListNode((int)e);
		if ( head == null ) {
			head = node;
		}
		
		node._next = null;
		SListNode last = head;
		while (last._next != null) {
			last = last._next;
		}
		last._next = node;
		size++;
	}

	public void insertAt( Object e, int idx ) {
		if ( head == null ) {
			return; // throw array range exception
		}
		
		SListNode last = head;
		for ( int i = 0; i < idx && last._next != null; i++) {
			last = last._next;
		}

		SListNode temp = last._next;
		SListNode node = new SListNode((int)e);
		last._next = node;
		node._next = temp;
		size++;
	}
	
	public void deleteFirst() {
		SListNode first = head;
		if (first == null) return;
		
		SListNode temp = first._next;
		first._next = null;
		first._data = 0;
		head = temp;
		size--;
	}
	
	public void deleteLast() {
		SListNode current = head;
		SListNode next = head._next;
		
		if ( current == null ) {
			return;
		} else if ( next == null ) {
			current._data = 0;
			head = null;
			size--;
			return;
		}
		
		while ( next._next != null) {
			current = current._next;
			next = next._next;
		}
		
		current._next = null;
		next._data = 0;
		size--;
	}
	
	/*
	 * Take the next node in some temp variable
	 * free the current node
	 * after freeing the current node, go to the next node with temp variable
	 * repeat this process for all nodes
	 */
	public void clear() {
		if ( head == null ) {
			return;
		}
		
		SListNode current = head;
		while ( current._next != null ) {
			SListNode temp = current._next;
			current._data = 0;
			current._next = null;
			current = temp;
		}
		
		current._data = 0;
		head = null;
		size = 0;
	}
	
	public boolean contains( Object o) {
		if ( o == null ) {
			return false;
		}
		
		SListNode current = head;
		while (current != null) {
			if ( current._data == (int) o ){
				return true;
			}
			current = current._next;
		}
		return false;
	}
	
	public boolean isEmpty() {
		return head == null;
	}
	
	public String toString() {
		if ( head == null ) {
			return null;
		}
		
		StringBuilder sb = new StringBuilder();
		sb.append('[');
		
		SListNode last = head;
		while ( last._next != null ) {
			sb.append(last._data);
			last = last._next;
			sb.append(',');
		}
		
		sb.append(last._data);
		sb.append(']');
		return sb.toString();
	}
	
	
	public static void main(String[] args) {
		SingleLinkedList l1 = new SingleLinkedList();
		l1.insertFront(10);
		l1.insertBack(20);
		l1.insertFront(30);
		
		System.out.println("List elements are:" + l1.toString());
		System.out.println("Size of list is:" + l1.size());
		System.out.println("List contains 30 : " + l1.contains( 30 ));
		System.out.println("List contains 50 : " + l1.contains( 50 ));
		
		l1.insertAt(60, 1);
		System.out.println("List elements are:" + l1.toString());
		System.out.println("Size of list is:" + l1.size());

		l1.deleteFirst();
		System.out.println("List elements are:" + l1.toString());
		System.out.println("Size of list is:" + l1.size());

		l1.insertBack(80);
		l1.insertBack(70);

		System.out.println("List elements are before deleteLast:" + l1.toString());
		l1.deleteLast();
		
		System.out.println("List elements are after deleteLast:" + l1.toString());
		System.out.println("Size of list is:" + l1.size());

		l1.clear();
		System.out.println("List elements are:" + l1.toString());
		System.out.println("Is List empty:" + l1.isEmpty());
		System.out.println("Size of list is:" + l1.size());


	}
}
