package basicjava;

class OuterStatic {
	int rollNo = 0;
	static int standard = 3;
	
	static class InnerStatic {
		public void accessMembers() {
			System.out.println("Printing " + standard);
		}
	}
}
public class StaticClassDemo {
	public static void main(String[] args) {
		OuterStatic.InnerStatic innerRef = new OuterStatic.InnerStatic();
		innerRef.accessMembers();
	}
}
