package basicjava;

public class InitializationDemoClass {
	{
		System.out.println("Empty block");
	}
	
	static {
		System.out.println("Static block");
	}
	
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		InitializationDemoClass t = new InitializationDemoClass();
		
		// note - static initializer will not be called
		InitializationDemoClass t1 = new InitializationDemoClass(); 
	}
}
