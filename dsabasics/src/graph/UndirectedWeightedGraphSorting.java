package graph;

import java.util.Scanner;
import java.util.*;

public class UndirectedWeightedGraphSorting {
    int[][] matrix;
    int[] visitedNodes;
    int[] route;
    
    int vertices;
    
    public void setVertices(int vertices) {
        this.vertices = vertices;    
    }
    
    public int[][] getGraph() {
    	return matrix;
    }
    
    // only for testing
    public int[] getRoute() {
    	return route;
    }
    
    public void buildAdjacencyMatrix(Scanner sc, boolean directed) {
        matrix = new int[vertices][vertices];
        visitedNodes = new int[vertices];
        route = new int[vertices];
        Arrays.fill(route, -1);
        
        int i = 0, j = 0;

        System.out.println("Enter the edges: (-1 for exit)");
        while (sc.hasNextInt()){
            System.out.println("Enter the edges: (-1 for exit)");
            int usrInput = sc.nextInt();
            if ( usrInput == -1) {
                break;
            }
            
            i = usrInput / 10;
            j = usrInput % 10;
            
            System.out.println("Enter the weight: ");
            int wt = sc.nextInt();
            
            if ( i < vertices && j < vertices ) {
                if ( directed ) {
                    matrix[i][j] = wt;
                }
                else {
                    matrix[i][j] = wt;
                    matrix[j][i] = wt;
                }
            }
        }        
    } 
    
    public void printGraph() {
        System.out.println("The adjacency matrix is: ");

        for (int x=0; x < vertices; x++) {
            for(int y =0; y < vertices; y++) {
                System.out.print(matrix[x][y] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        UndirectedWeightedGraphSorting graph = new UndirectedWeightedGraphSorting();

        System.out.println("Enter the number of vertices:");
        graph.setVertices(sc.nextInt());
        
        System.out.println("Is it directed graph? ");
        boolean directed = sc.nextBoolean();
        graph.buildAdjacencyMatrix(sc, directed);
        
        graph.printGraph();
        System.out.println("Traversal using iterative method is:");
    }    
}