package graph;

import java.util.Scanner;
import java.util.*;

public class TopologicalSortDFS {
    int[][] matrix;
    int[] visitedNodes;
    int vertices;
    
    void setVertices(int vertices) {
        this.vertices = vertices;    
    }
    
    public void buildAdjacencyMatrix(Scanner sc, boolean directed) {
        matrix = new int[vertices][vertices];
        visitedNodes = new int[vertices];
        
        matrix[0][2] = 1;
        matrix[1][2] = 1;
        matrix[1][3] = 1;
        matrix[2][4] = 1;
        matrix[3][5] = 1;
        matrix[4][7] = 1;
        matrix[4][5] = 1;
        matrix[5][6] = 1;
    } 
    
    public void topologicalSort(int start) {
        Stack<Integer> output = new Stack<Integer>();
        if ( visitedNodes[start] != 1) {
            visitedNodes[start] = 1;
            Stack<Integer> stack = new Stack<Integer>(); 
            stack.push(start);
            
            while (! stack.empty()) {
                int x = stack.peek();
                int y;
                for ( y = 0; y < vertices; y++) {
                    if ( matrix[x][y] == 1 && visitedNodes[y] != 1) {
                        stack.push(y);
                        visitedNodes[y] = 1;
                        System.out.println("Visiting node: (" + y + ")");
                        break;
                    }
                }
                
                if(y == vertices) {
                    output.push(stack.pop());
                }
                
                if ( stack.empty()) {
                    for ( int i=0; i < visitedNodes.length; i++ ) {
                        if (visitedNodes[i] != 1) {
                            visitedNodes[i] = 1;
                            stack.push(i);
                            break;
                        }
                    }
                }
            }
        }

        // Print output
        String str = "ABCDEFGH";
        char[] opArr = str.toCharArray();
        while (! output.empty()){
            System.out.print(" " + opArr[output.pop()]);
        }
    }
    
    public void printGraph() {
        System.out.println("The adjacency matrix is: ");
        for (int x=0; x < vertices; x++) {
            System.out.println();
            for(int y =0; y < vertices; y++) {
                System.out.print(matrix[x][y] + " ");
            }
        }
        System.out.println();
    }
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        TopologicalSortDFS graph = new TopologicalSortDFS();

        graph.setVertices(8);
        graph.buildAdjacencyMatrix(sc, true);
        
        graph.printGraph();
        System.out.println("Topological Sort order is:");
        graph.topologicalSort(1);
    }    
}