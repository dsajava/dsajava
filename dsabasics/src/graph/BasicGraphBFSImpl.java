package graph;

import java.util.Scanner;
import java.util.*;

/*
 * Breadth first search algorithm
 * It is a level wise traversal algorithm
 * Given a start point, it will traverse vertices reachable from start point
 * 
 * The graph has been constructed using adjacency matrix
 */
public class BasicGraphBFSImpl {
    int[][] matrix;
    int[] visitedNodes;
    int[] route;
    
    int vertices;
    
    public void setVertices(int vertices) {
        this.vertices = vertices;    
    }
    
    public int[][] getGraph() {
    	return matrix;
    }
    
    // only for testing
    public int[] getRoute() {
    	return route;
    }
    
    public void buildAdjacencyMatrix(Scanner sc, boolean directed) {
        matrix = new int[vertices][vertices];
        visitedNodes = new int[vertices];
        route = new int[vertices];
        Arrays.fill(route, -1);
        
        int i = 0, j = 0;

        System.out.println("Enter the edges: (-1 for exit)");
        while (sc.hasNextInt()){
            System.out.println("Enter the edges: (-1 for exit)");
            int usrInput = sc.nextInt();
            if ( usrInput == -1) {
                break;
            }
            
            // assume the number of vertices to be less than 10
            i = usrInput / 10;
            j = usrInput % 10;

            if ( i < vertices && j < vertices ) {
                if ( directed ) {
                    matrix[i][j] = 1;
                }
                else {
                    matrix[i][j] = 1;
                    matrix[j][i] = 1;
                }
            }
        }        
    } 
    
    public void bfsIterative(int start) {
    	int idx = 0;
        if ( visitedNodes[start] != 1) {
            visitedNodes[start] = 1;
            Queue<Integer> queue = new LinkedList<Integer>(); 
            queue.add(start);
            
            while (! queue.isEmpty()) {
                int x = queue.remove();
                for ( int y = 0; y < vertices; y++) {
                    if ( matrix[x][y] == 1 && visitedNodes[y] != 1) {
                        queue.add(y);
                        visitedNodes[y] = 1;
                        route[idx++] = y;
                        System.out.println("Visiting node: (" + y + ")");
                    }
                }
            }
        }
    }
    
    public void printGraph() {
        System.out.println("The adjacency matrix is: ");

        for (int x=0; x < vertices; x++) {
            for(int y =0; y < vertices; y++) {
                System.out.print(matrix[x][y] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        BasicGraphBFSImpl graph = new BasicGraphBFSImpl();

        System.out.println("Enter the number of vertices:");
        graph.setVertices(sc.nextInt());
        
        System.out.println("Is it directed graph? ");
        boolean directed = sc.nextBoolean();
        graph.buildAdjacencyMatrix(sc, directed);
        
        graph.printGraph();
        System.out.println("Traversal using iterative method is:");
        graph.bfsIterative(0);
    }    
}