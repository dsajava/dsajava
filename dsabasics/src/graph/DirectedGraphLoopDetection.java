package graph;

import java.util.Scanner;
import java.util.Stack;

public class DirectedGraphLoopDetection {
    int[][] matrix;
    int[] route;
    int[] visitedNodes;
    int loopCount;
    int vertices;
    
    public void setVertices(int vertices) {
        this.vertices = vertices;    
    }

    public int[][] getGraph() {
    	return matrix;
    }
    
    // only for testing
    public int[] getRoute() {
    	return route;
    }
    
    public int getLoopCount() {
    	return loopCount;
    }
    
    public void buildAdjacencyMatrix(Scanner sc, boolean directed) {
        matrix = new int[vertices][vertices];
        route = new int[vertices];
        visitedNodes = new int[vertices];
        
        int i = 0, j = 0;

        System.out.println("Enter the edges: (-1 for exit)");
        while (sc.hasNextInt()){
            System.out.println("Enter the edges: (-1 for exit)");
            int usrInput = sc.nextInt();
            if ( usrInput == -1) {
                break;
            }
            
            i = usrInput / 10;
            j = usrInput % 10;

            if ( i < vertices && j < vertices ) {
                if ( directed ) {
                    matrix[i][j] = 1;
                }
                else {
                    matrix[i][j] = 1;
                    matrix[j][i] = 1;
                }
            }
        }        
    } 

    public boolean hasCyclicLoop(int start) {
        if ( visitedNodes[start] != 1) {
        	visitedNodes[start] = 1;
            Stack<Integer> stack = new Stack<Integer>(); 
            stack.push(start);
            
            while (! stack.empty()) {
                int x = stack.peek();
                int y;
                for ( y = 0; y < vertices; y++) {
                    if ( matrix[x][y] == 1 ) {
                    	if ( stack.contains(y)) {
                    		loopCount++;
                    	}
                    	
                    	if ( visitedNodes[y] != 1) {
                        	stack.push(y);
                        	visitedNodes[y] = 1;
                        	System.out.println("Visiting node: (" + y + ")");
                        	break;	                    		
                    	}
                    }
                }
                if(y == vertices) {
                    stack.pop();
                }
            }
        }
        return loopCount > 0;
    }
    
    public void printGraph() {
        System.out.println("The adjacency matrix is: ");

        for (int x=0; x < vertices; x++) {
            for(int y =0; y < vertices; y++) {
                System.out.print(matrix[x][y] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

}
