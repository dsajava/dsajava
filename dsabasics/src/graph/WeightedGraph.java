package graph;

import java.util.Collections;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Comparator;

class Edge implements Comparable<Edge>  {
    int source;
    int destination;
    int weight;

    public Edge(int source, int destination, int weight) {
        this.source = source;
        this.destination = destination;
        this.weight = weight;
    }
    
    public int getWeight() {
        return weight;
    }
    
    public int getSource() {
        return source;
    }
    
    public int getDestination() {
        return destination;
    }
    
    /*
     * (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     * This will be used in case of weighted graph
     */
    @Override
    public int compareTo(Edge edge) {
        return this.getWeight() - edge.getWeight(); 
    }
}

class EdgeComparator implements Comparator<Edge>{ 
    public int compare(Edge e1, Edge e2) { 
        return e1.getWeight() - e2.getWeight();
    } 
} 

public class WeightedGraph {
    int vertices;
    boolean isDirected = false;
    
    LinkedList<Edge> [] adjacencylist;
    ArrayList<Edge> edgesArray = new ArrayList<Edge>();
    PriorityQueue<Edge> edgesQueue = new PriorityQueue<Edge>(10, new EdgeComparator());
    
    int[] parents;

    @SuppressWarnings("unchecked")
	WeightedGraph(int vertices) {
        this.vertices = vertices;
        adjacencylist = new LinkedList[vertices];
        parents = new int[vertices];

        for (int i = 0; i < vertices ; i++) {
            adjacencylist[i] = new LinkedList<Edge>();
        }
        
        for (int i=0; i < vertices; i++) {
            parents[i] = -1;
        }
    }
    
    public void setDirected ( boolean isDirected ) {
        this.isDirected = isDirected;
    }

    public void addEgde(int source, int destination, int weight ) {
        if ( isDirected ) {
            Edge edge = new Edge(source, destination, weight);
            adjacencylist[source].addFirst(edge); 
            edgesArray.add(edge); 
            edgesQueue.add(edge);
        }
        else { 
            Edge edge1 = new Edge(source, destination, weight);
            Edge edge2 = new Edge(destination, source, weight);
            adjacencylist[source].addFirst(edge1); 
            adjacencylist[source].addFirst(edge2); 

            edgesArray.add(edge1); 
            edgesQueue.add(edge1);
        }
    }
    
    public void sortEdges() {
        Collections.sort(edgesArray);
    }
    
    public void printSortedGraph() {
        for (Edge edge : edgesArray) {
            System.out.println("Vertex - " + edge.source + " to " + edge.destination + " with weight " + edge.weight);
        }
    }
    
    void printPriorityQueue() {
        System.out.println("Printing Priority queue contents");
        
        while (! edgesQueue.isEmpty() ) {
            Edge edge = edgesQueue.poll();
            System.out.println("Vertex - " + edge.source + " to " + edge.destination + " with weight " + edge.weight);
        }
    }

    public void printGraph(){
        for (int i = 0; i <vertices ; i++) {
            LinkedList<Edge> list = adjacencylist[i];
                for (int j = 0; j <list.size() ; j++) {
                    System.out.println("vertex-" + i + " is connected to " +
                        list.get(j).destination + " with weight " +  list.get(j).weight);
            }
        }
    }
    
    private int find( int index) {
        if ( parents[index] == -1) {
            return index;
        }
        else {
            return find( parents[index]);
        }
    }

    private void union( int x, int y) {
        int xloc = find(x);
        int yloc = find(y);
        
        parents[xloc] = yloc;
    }    
    
    private boolean hasCycle( Edge edge ) {
        int x = find( edge.getSource());
        int y = find( edge.getDestination());
            
        if ( x == y ) {
            return true;
        }
        else {
            union(x, y);
        }
        return false;    
    }
    
    public void createMST () {
        for (Edge edge : edgesArray) {
            if (! hasCycle (edge )) {
                System.out.println("Edge source: " + edge.getSource() + " Dest: " + edge.getDestination() + " Weight: " + edge.getWeight());
            }
        }
    }

    public static void testGraphUsingKruskalAlgo() {
        int vertices = 6;
        WeightedGraph graph = new WeightedGraph(vertices);
        graph.setDirected(false);
        
        graph.addEgde(0, 1, 3);
        graph.addEgde(0, 5, 4);
        graph.addEgde(1, 5, 6);
        graph.addEgde(1, 2, 10);
        graph.addEgde(2, 5, 8);
        graph.addEgde(2, 4, 18);
        graph.addEgde(2, 3, 2);
        graph.addEgde(4, 3, 17);
        graph.addEgde(3, 5, 11);

        graph.printGraph();
        graph.sortEdges();
        System.out.println("Sorted graph below:");
        graph.printSortedGraph();
        graph.createMST();
    }
    
    
    public static void testGraphUsingPriorityQueue() {
        int vertices = 6;
        WeightedGraph graph = new WeightedGraph(vertices);
        graph.setDirected(false);
        
        graph.addEgde(0, 1, 3);
        graph.addEgde(0, 5, 4);
        graph.addEgde(1, 5, 6);
        graph.addEgde(1, 2, 10);
        graph.addEgde(2, 5, 8);
        graph.addEgde(2, 4, 18);
        graph.addEgde(2, 3, 2);
        graph.addEgde(4, 3, 17);
        graph.addEgde(3, 5, 11);

        graph.printGraph();
        graph.printPriorityQueue();
    }

    public static void main(String[] args) {
        testGraphUsingKruskalAlgo();
        testGraphUsingPriorityQueue();
    }
}