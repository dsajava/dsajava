package graph;

import java.util.Scanner;
import java.util.*;

public class BasicGraphImpl {
    int[][] matrix;
    int[] visitedNodes;
    int[] visitedNodesForIterative;
    
    int vertices;
    
    void setVertices(int vertices) {
        this.vertices = vertices;    
    }
    
    public void buildAdjacencyMatrix(Scanner sc, boolean directed) {
        matrix = new int[vertices][vertices];
        visitedNodes = new int[vertices];
        visitedNodesForIterative = new int[vertices];
        
        int i = 0, j = 0;

        System.out.println("Enter the edges: (-1 for exit)");
        while (sc.hasNextInt()){
            System.out.println("Enter the edges: (-1 for exit)");
            int usrInput = sc.nextInt();
            if ( usrInput == -1) {
                break;
            }
            
            i = usrInput / 10;
            j = usrInput % 10;

            if ( i < vertices && j < vertices ) {
                if ( directed ) {
                    matrix[i][j] = 1;
                }
                else {
                    matrix[i][j] = 1;
                    matrix[j][i] = 1;
                }
            }
        }        
    } 
    
    public void dfsTraversal(int x) {
        if ( visitedNodes[x] != 1) {
            visitedNodes[x] = 1;
            
            for (int y = 0; y < vertices; y++) {
                if (matrix[x][y] == 1 && visitedNodes[y] != 1) {
                    System.out.println("Visiting node: (" + y + ")");
                    dfsTraversal(y);
                }
            }
        }                
    }
    
    public void dfsIterative(int start) {
        if ( visitedNodesForIterative[start] != 1) {
            visitedNodesForIterative[start] = 1;
            Stack<Integer> stack = new Stack<Integer>(); 
            stack.push(start);
            
            while (! stack.empty()) {
                int x = stack.peek();
                int y;
                for ( y = 0; y < vertices; y++) {
                    if ( matrix[x][y] == 1 && visitedNodesForIterative[y] != 1) {
                        stack.push(y);
                        visitedNodesForIterative[y] = 1;
                        System.out.println("Visiting node: (" + y + ")");
                        break;
                    }
                }
                if(y == vertices) {
                    stack.pop();
                }
            }
        }
    }
    
    public void printGraph() {
        System.out.println("The adjacency matrix is: ");
        for (int x=0; x < vertices; x++) {
            System.out.println();
            for(int y =0; y < vertices; y++) {
                System.out.print(matrix[x][y] + " ");
            }
        }
        System.out.println();
    }
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        BasicGraphImpl graph = new BasicGraphImpl();

        System.out.println("Enter the number of vertices:");
        graph.setVertices(sc.nextInt());
        
        System.out.println("Is it directed graph? ");
        boolean directed = sc.nextBoolean();
        graph.buildAdjacencyMatrix(sc, directed);
        
        graph.printGraph();
        graph.dfsTraversal(1);
        System.out.println("Traversal using iterative method is:");
        graph.dfsIterative(1);
    }    
}