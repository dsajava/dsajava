package doublelinkedlist;
class Node {
	private int _data;
	private Node _next;
	private Node _prev;
	
	Node(int data) {
		_data = data;
		_next = null;
		_prev = null;
	}
	
	public Node getNext() {
		return _next;
	}
	
	public Node getPrev() {
	    return _prev;
	}
	
	public void setNext( Node node ) {
		_next = node;
	}
	
	public void setPrev( Node node ) {
	    _prev = node;
	}
	
	public int getData() {
		return _data;
	}
	
	public void setData( int data ) {
		_data = data;
	}
	
}
	
public class DoubleLinkedList {
    Node head;
	    
    public void insert ( int data ) {
        Node node = new Node(data);
        node.setNext( null );

        if ( head == null ) {
            node.setPrev(null);
            head = node;
            return;
        }

        Node curr = head;
        while ( curr.getNext() != null ) {
            curr = curr.getNext();
        }
            
        curr.setNext(node);
        node.setPrev(curr);
        return;
    }
    
    
    @SuppressWarnings("unused")
	public void deleteFirst () {
    	if ( head == null ) {
    		return;
    	}
    	
    	Node tmp = head;
    	head = head.getNext();
    	if ( head != null ) {
    		head.setPrev(null);
    	}
    	
    	tmp = null;
    	return;
    }
    
    
	public void reverseDLL () {
	    if ( head == null ) {
	        return;
	    }
	    
	    Node curr = head;
	    Node prev = null;
	    Node next = null;
	    
	    while ( curr != null ) {
	        prev = curr.getPrev();
	        next = curr.getNext();
	        curr.setNext(prev);
	        curr.setPrev(next);
	        curr = curr.getPrev();
	    }
	    
	    if ( prev != null ){	    
	        head = prev.getPrev();
	    }
	}
	    
    public static void main( String[] args ) {
	    DoubleLinkedList dl = new DoubleLinkedList();
	    dl.insert(10);
	    dl.insert(20);
	    dl.insert(30);
	    dl.insert(5);
	    
	    dl.reverseDLL();
	    
	    Node curr = dl.head;
	    System.out.println("List elements are:");
	    while ( curr.getNext() != null ) {
	        System.out.print(curr.getData() + ",");
	        curr = curr.getNext();
	    }
	    System.out.print(curr.getData());
	    System.out.println();
	    
	    Node prev = curr;
	    System.out.println("Reverse of the list printed:");
	    while ( prev != null ) {
            System.out.print(prev.getData() + ",");
            prev = prev.getPrev();
	    }
        System.out.println();

        dl.deleteFirst();

        curr = dl.head;
	    System.out.println("List elements after first element deletion are:");
	    while ( curr.getNext() != null ) {
	        System.out.print(curr.getData() + ",");
	        curr = curr.getNext();
	    }
	    System.out.println(curr.getData());
	}
}
