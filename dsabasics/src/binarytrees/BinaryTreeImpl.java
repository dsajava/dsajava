package binarytrees;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

import queues.BasicQueue;

public class BinaryTreeImpl {
	BinaryTreeNode _root;
	
	BinaryTreeImpl( BinaryTreeNode node ) {
		_root = node;
	}
	
	public void insert( BinaryTreeNode root, Integer key ) {
		BinaryTreeNode node = new BinaryTreeNode( key );
		
		if ( root == null ) {
			root = node;
			return;
		}

		Queue<BinaryTreeNode> queue = new LinkedList<BinaryTreeNode>();
		queue.add(root);
		while ( ! queue.isEmpty() ){
			BinaryTreeNode current = queue.peek();
			queue.remove();
			
			if ( current.left != null ) {
				queue.add( current.left );
			}
			else {
				current.left = node;
				break;
			}
			
			if ( current.right != null ) {
				queue.add( current.right );
			}
			else {
				current.right = node;
				break;
			}
			
		}
	}
	
	public void levelOrderTraversal ( BinaryTreeNode root ) {
		if ( root == null ) {
			return;
		}
		
		Queue<BinaryTreeNode> queue = new LinkedList<BinaryTreeNode>();
		queue.add( root );
		StringBuffer sb = new StringBuffer();
		sb.append('[');
		
		while ( ! queue.isEmpty() ) {
			BinaryTreeNode current = queue.peek();
			sb.append( current.data );
			sb.append( ',' );
			
			if ( current.left != null ) queue.add( current.left );
			if ( current.right != null ) queue.add( current.right );
			
			queue.remove(current);
		}
		sb.append( ']' );
		System.out.println( "Level order traversal: " + sb.toString());
	}
	
	
	public void preOrder( BinaryTreeNode root ) {
		if ( root == null ) return;
		System.out.print(root.data + ",");
		preOrder(root.left);
		preOrder(root.right);
	}
	
	public void preOrderTraversal( BinaryTreeNode root ) {
		if ( root == null ) {
			return;
		}
		
		Stack<BinaryTreeNode> stack = new Stack<BinaryTreeNode>();
		StringBuffer sb = new StringBuffer();
		sb.append('[');

		while ( true ) {
			while ( root != null ) {
				sb.append( root.data );
				sb.append(',');
				stack.push(root);
				root = root.left;
			}

			if ( stack.isEmpty() ) break;
			
			root = stack.pop();
			root = root.right;
		}
		
		sb.append(']');
		System.out.println("Pre order traversal: " + sb.toString());
	}
	
	public void inOrder ( BinaryTreeNode root ) {
		if ( root == null ) return;
		
		inOrder(root.left);
		System.out.print(root.data + ",");
		inOrder(root.right);
	}
	
	public void inOrderTraversal( BinaryTreeNode root ) {
		if ( root == null ) {
			return;
		}
		
		Stack<BinaryTreeNode> stack = new Stack<BinaryTreeNode>();
		StringBuffer sb = new StringBuffer();
		sb.append('[');

		while ( true ) {
			while ( root != null ) {
				stack.push(root);
				root = root.left;
			}

			if ( stack.isEmpty() ) break;
			
			root = stack.pop();
			sb.append( root.data );
			sb.append(',');
			root = root.right;
		}
		
		sb.append(']');
		System.out.println("In order traversal: " + sb.toString());
	}
	
	public void postOrderTraversal( BinaryTreeNode root ) {
		if ( root == null ) {
			return;
		}
		
		Stack<BinaryTreeNode> stack1 = new Stack<BinaryTreeNode>();
		Stack<BinaryTreeNode> stack2 = new Stack<BinaryTreeNode>();
		stack1.push(root);
		
		StringBuffer sb = new StringBuffer();
		sb.append('[');

		while (! stack1.isEmpty()){
			BinaryTreeNode node = stack1.pop();
			stack2.push(node);
			if ( node.left != null) stack1.push(node.left);
			if ( node.right != null) stack1.push(node.right);
		}
		
		while (! stack2.isEmpty()){
			sb.append(stack2.pop().data);
			sb.append(',');
		}

		sb.append(']');
		System.out.println("Post order traversal: " + sb.toString());
	}
	
	@SuppressWarnings("unused")
	public int findMax (BinaryTreeNode root) {
		int rootVal;
		int max = Integer.MIN_VALUE , left = Integer.MIN_VALUE, right = Integer.MIN_VALUE;
		if ( root != null ) {
			rootVal = root.data;
			if ( root.left != null) left = findMax(root.left);
			if ( root.right != null) right = findMax(root.right);
			
			max = left > right ? left : right;
			max = max > rootVal ? max : rootVal;
		}
		return max;
	}
	
	public int findMaxInOrder( BinaryTreeNode root ) {
		if ( root == null ) {
			return 0;
		}
		
		int max = 0;
		
		Stack<BinaryTreeNode> stack = new Stack<BinaryTreeNode>();
		while ( true ) {
			while ( root != null ) {
				stack.push(root);
				root = root.left;
			}

			if ( stack.isEmpty() ) break;
			
			root = stack.pop();
			max = max > root.data ? max : root.data;
			root = root.right;
		}
		return max;
	}

	
	public static void main (String[] args) {
		BinaryTreeNode root = new BinaryTreeNode( 10 );
		BinaryTreeImpl tree = new BinaryTreeImpl( root );
		tree.insert( root, 20 );
		tree.insert( root, 25 );
		tree.insert( root, 9 );
		tree.insert( root, 40 );
		tree.insert(root, 20);
		tree.levelOrderTraversal( root );
		tree.inOrderTraversal(root);
		tree.inOrder(root);
		System.out.println(" ");

		tree.preOrderTraversal(root);
		tree.preOrder(root);
		System.out.println(" ");
		tree.postOrderTraversal(root);
		
		System.out.println("Max node value is :" + tree.findMax(root) );
		System.out.println("Max node value using inOrder traversal is :" + tree.findMaxInOrder(root) );

	}
	
}
