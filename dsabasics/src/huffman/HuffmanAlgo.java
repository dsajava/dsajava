package huffman;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

class Node {
    String data;
    int freq;
    Node left = null, right = null;
    
    Node() {
    	
    }
    
    Node(String data, int freq, Node left, Node right) {
    	this.data = data;
    	this.freq = freq;
    	this.left = left;
    	this.right = right;
    }
    
    public int getFreq() {
        return freq;
    }
    
    public String getData() {
        return data;
    }
    
    public void setFreq(int freq) {
        this.freq = freq;
    }
    
    public void setData(String data) {
        this.data = data;
    }
    
    public String toString() {
        return data + "=" + freq;
    }
}

class MinHeap {
    int index = -1;
    final static int CAPACITY = 15;
    Node[] array;
    int capacity = CAPACITY;
    
    MinHeap() {
        this(CAPACITY);    
    }
    
    MinHeap(int capacity) {
        this.capacity = capacity;
        array = new Node[capacity];
    }
    
    private int parent(int i) {
    	return (i - 1)/2;
    }
    
    private int leftChild(int i) {
    	return 2*i + 1;
    }
    
    private int rightChild(int i) {
    	return 2*i + 2;
    }
    
    public void enqueue( Node node ) {
    	if ( index == capacity ) {
    		System.out.println("Heap full error");
    		return;
    	}
    	
        array[++index] = node;
        // bubble up the smallest node
        for(int i = index; i > 0; i = (i-1)/2) {
            // where j is parent of the index node where we have inserted
            int j = (i - 1)/2;
            // if parent is greater than node added - swap
            if ( array[j].getFreq() > node.getFreq() ) {
                Node tmp = array[j];
                array[j] = node;
                array[i] = tmp;
            }
            else {
                break;
            }
        }
    }
    
    public int size() {
    	return index;
    }
    
    public boolean isEmpty() {
    	return index == -1;
    }
    
    private void heapifyDown(int i) {
        int smallest = i;
        while (true) {
        	// get left and right childs
    	    int left = leftChild(smallest);
    	    int right = rightChild(smallest);
    	
        	// compare A[i] with its left and right
        	// and find the smaller value
    	    if ( left < index && array[left].getFreq() < array[i].getFreq() ){
    		    smallest = left;
    	    }
    	
    	    if ( right < index && array[right].getFreq() < array[smallest].getFreq()) {
    		    smallest = right;
    	    }
    	
    	    if ( smallest != i ) {
    		    Node tmp = array[i];
    		    array[i] = array[smallest];
    		    array[smallest] = tmp;
    		    i = smallest;
        	}
        	else {
        	    break;
        	}
        }
    }
    
    public Node dequeue () {
    	if ( isEmpty()) {
    		System.out.println("Heap is empty");
    		return null;
    	}
    	
    	Node value = array[0];
    	array[0] = array[index];
    	index--;
    	heapifyDown(0);
		return value;
    }
    
    public void printAll() {
        for ( int i=0; i <= index; i++ ) {
            System.out.print( array[i].toString() + ", ");
        }
        System.out.println();
    }
}

public class HuffmanAlgo {
    
    private static int[] decodeFrequency(char[] array) {
        int[] freqArr = new int[127];
        //int ascii = 0;
        for ( char c : array ) {
            //ascii = (int) c;
            //freqArr[ascii]++;
        	freqArr[c - 'A']++;
        }
        return freqArr;
    }

    private static void encode(Node root, String str, Map<String, String> map) {
		if (root == null ) {
			return;
		}
		
		// found leaf node
		if ( root.left == null && root.right == null ) {
			map.put(root.data, str);
		}
		
		encode(root.left, str + "0", map);
		encode(root.right, str + "1", map);
	}

    public static void generateHuffmanCodes(char[] array) {
        int[] freqArr = decodeFrequency(array);

        // Store the nodes with char and its frequency in minHeap
        MinHeap mHeap = new MinHeap(20);
        for (int i=0, k = 0; i < freqArr.length; i++, k =0 ) {
            k = i;
            char letter = (char)k;
            
            if ( freqArr[i] > 0 ) {
                Node node = new Node();
                node.setData(letter + "");
                node.setFreq(freqArr[i]);
                mHeap.enqueue(node);
            }
        }
        
        System.out.println("Printing MinHeap");
        mHeap.printAll();

        while ( mHeap.size() != 0 ) {
        	Node left = mHeap.dequeue();
        	Node right = mHeap.dequeue();
        	
        	int freqSum = left.freq + right.freq;
        	String data = left.getData() + right.getData();
        	Node node = new Node(data, freqSum, left, right);
        	mHeap.enqueue(node);
        }
        
        System.out.println("Printing Huffman Tree root node");
        mHeap.printAll();

        // traverse through the Huffman Tree Root node and set the code
        Map<String, String> map = new HashMap<String, String>();
        Node root = mHeap.dequeue();
        encode(root, "", map);
        final String space = " ";
        for ( String key : map.keySet()) {
        	if ( key.equals(space)) {
            	System.out.println("Space=" + map.get(key));
        	}
        	else { 
        		System.out.println(key + "=" + map.get(key));
        	}
        }
    }
    

	public static void main(String[] args) {
        System.out.println("Enter your string phrase: ");
        Scanner sc = new Scanner(System.in);
        String strphrase = sc.nextLine().toUpperCase();        
        System.out.println(strphrase);
        generateHuffmanCodes(strphrase.toCharArray());
    }
}