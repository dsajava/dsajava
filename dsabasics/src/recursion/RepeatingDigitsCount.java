package recursion;

public class RepeatingDigitsCount {
	
	public static int countNoOfRepetition( int num, int digit ){
		if ( num <= 0) {
			return 0;
		}
		else if ( num % 10 == digit ){
			return 1 + countNoOfRepetition( num/10, digit );
		}
		else {
			return countNoOfRepetition( num/10, digit );
		}
	}
		
	public static int countNoOfRepetition( int num, int digit, int count ) {
		if ( num == 0 ) {
			return count;
		}
		else {
			int k = num % 10;
			if ( k == digit ) {
				count++;
			}
			return countNoOfRepetition( num/10, digit, count);
		}
	}
	
	public static void main( String[] args ) {
		System.out.println("no of times 8 occurs is:" + countNoOfRepetition( 288976588, 8, 0));
		System.out.println("no of times 8 occurs is:" + countNoOfRepetition( 288976588, 8));
		System.out.println("no of times 8 occurs is:" + countNoOfRepetition( 1234567, 8));
		System.out.println("no of times 8 occurs is:" + countNoOfRepetition( 888, 8));


	}

}
