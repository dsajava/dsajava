package recursion;

/*
 * Using recursion, reverse an array
 * use a second array to store the reversed array
 * 
 */

public class ArrayReversal {

	public static void reverseArray( int[] array, int[] revArr, int fwd, int back ) {
		if ( back < 0) {
			return;
		}
		else {
			revArr[fwd++] = array[back--];
			reverseArray( array, revArr, fwd, back );
		}
	}
	
	
	public static void printArr(int[] reverseArr) {
		System.out.print("Reverse array element is: {");
		for ( int i : reverseArr ) {
			System.out.print( " " + i);
		}
		System.out.println("}");
	}
	
	public static void main( String[] args ){
		int[] arr = { 2, 3, 4, 5, 8};
		int[] reverseArr = new int[arr.length];
		reverseArray( arr, reverseArr, 0, arr.length -1);
		printArr(reverseArr);
		
		int[] arr1 = { };
		int[] revA1 = new int[arr1.length];
		
		reverseArray( arr1, revA1, 0, arr1.length -1);
		printArr(revA1);
	}
}
