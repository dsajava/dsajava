package recursion;

/*
 * Print n Fibonacci series (1,1,2,3,5,8,13,21...)
 * using recursion
 * Note: fib series nth item is sum of n-1 and n-2th items
 */

public class Fibonacci {
	
	public static int fib ( int num ) {
		if ( num <= 1 ) {
			return 1;
		}
		else if ( num == 2 ) {
			return 1;
		}
		else {
			return fib( num - 1 ) + fib( num - 2 );
		}
	}
	
	public static void main( String[] args) {
		for ( int i = 1; i < 10; i++) {
			System.out.print(" " + fib(i));
		}
		
	}
}
