package recursion;

public class StringSearch {
    static int count = 0;

    public static int countNoOfRecurrence( String s, String key ) {

        if ( s.length() == 0 ) {
            return count;
        }
        else {
            if ( s.startsWith(key) ) {
                count++;
                countNoOfRecurrence( s.substring(key.length()), key ); // this won't work if the intent is to count the key repetition combinations
            }
            else {
                countNoOfRecurrence( s.substring(1), key );
            }
        }
        return count;
    }

    public static void main ( String[] args ) {
        String a = "Hi";
        System.out.println("No of count of e is:" +  countNoOfRecurrence(a, "Hi"));
    }
}