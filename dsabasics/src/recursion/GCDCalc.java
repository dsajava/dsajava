package recursion;

public class GCDCalc {
	
	public static int gcdCal ( int p, int q ){
		if ( p == 0 ) {
			return q;
		}
		else {
			return gcdCal( q % p, p);
		}
		
	}
	
	public static void main( String[] args ) {
		System.out.println( "The GCD of 18, 48 is:" + gcdCal( 18, 48 ) );
	}
}
