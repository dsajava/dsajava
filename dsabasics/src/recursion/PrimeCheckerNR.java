package recursion;

public class PrimeCheckerNR {
    public static boolean isPrime (int num) {
        
        for ( int i = 2; i <= num/2 ; i++ ) {
            if ( num % i == 0) {
                return false;
            }   
        }
        
        return true;
    }
    
    public static void main( String[] args ) {
        System.out.println("3 is prime : " + isPrime(3));
        System.out.println("6 is prime : " + isPrime(6) );
    }
}