package recursion;
import java.util.Scanner;
/**
 * This program takes n input from the user, and forms Pascal's Triangle in n number of rows that the user entered.
 */

public class PascalsTriangle {

	/**
    *
    * @param row rows in Pascal's Triangle
    * @param col columns in Pascal's Triangle
    * @return values in the Pascal's Triangle
    */
   private static int pascalTriangle(int row, int col) {
	   // first and last element of a row will be 1
	   // pascal(i,0) and pascal(i,i) both should be 1
       if (col==0 || col == row) {
    	   return 1;
       }
       else {
    	   //create the values in Pascal's Triangle
    	   return pascalTriangle(row - 1, col - 1) + pascalTriangle(row - 1, col); 
       }
   }

   /**
    *
    * @param n the input from the user 
    */
   public static void printTriangle(int n){
	   for (int row = 0; row < n; row++) {
		 //  for (int k =0; k < n - row; k++) {
    	//	   System.out.print(" ");
    	//   }

           for (int col=0; col <= row; col++){
               System.out.print(pascalTriangle(row,col) + " ");
           }
           System.out.println();
       }
   }

   @SuppressWarnings("resource")
   public static void main(String[] args){
	   //Scanner scanner = new Scanner(System.in);
	   System.out.println("Enter an integer: ");
	   //int n = scanner.nextInt(); //the number of rows created in the Pascal's Triangle
	   printTriangle(5); //print Pascal's Triangle
   }



}
