package graphTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Scanner;

import org.junit.Test;

import graph.DirectedGraphLoopDetection;

public class DirectedGraphLoopDetectionTest {
	DirectedGraphLoopDetection graph = new DirectedGraphLoopDetection();

	public void setupWithOneLoop() {
		String data = "5\r\ntrue\r\n01\r\n12\r\n23\r\n31\r\n24\r\n-1\r\n";
		InputStream in = System.in;
		Scanner scanner = null;
		try {
	        System.out.println("Enter the number of vertices:");	        
			
			System.setIn(new ByteArrayInputStream(data.getBytes()));
			scanner = new Scanner(System.in);
	        graph.setVertices(Integer.parseInt(scanner.nextLine()));

	        System.out.println("Is it directed graph? ");
	        boolean directed = Boolean.parseBoolean(scanner.nextLine());

	        graph.buildAdjacencyMatrix(scanner, directed);
		}
		finally {
			System.setIn(in);
			scanner.close();
		}
	}
	
	@Test
	public void testBuildAdjacencyMatrix() {
        int arr[][] = {{0,1,0,0,0}, {0,0,1,0,0}, {0,0,0,1,1}, {0,1,0,0,0}, {0,0,0,0,0}};
        setupWithOneLoop();
        assertArrayEquals(null, arr, graph.getGraph());
        graph.printGraph();
	}
	
	@Test
	public void testHasCyclicLoop() {
        setupWithOneLoop();

		assertTrue(graph.hasCyclicLoop(0));
		assertEquals(1, graph.getLoopCount());
	}
	
	
	static public void assertArrayEquals(String message,
            int[][] expected,
            int[][] actual) {

		// If both arrays are null, then we consider they are equal
		if (expected == null && actual == null) {
			return;
		}

		// We test to see if the first dimension is the same.
		if (expected.length != actual.length) {
			fail(message + ". The array lengths of the first dimensions aren't the same.");
		}

		// We test every array inside the 'outer' array.
		int len = expected.length;
		int i = 0;
		for (i=0; i < len; i++) {
			System.out.println();
			assertTrue(message + ". Array no." + i + " in expected and actual aren't the same.",
					Arrays.equals(expected[i], actual[i]));
		}
	}


}
