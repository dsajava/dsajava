package tacos;

import org.springframework.stereotype.Component;

@Component
public class Laptop {
	private String brand;
	private int id;
	
	public String getBrand() {
		return brand;
	}
	
	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Laptop [brand=" + brand + ", id=" + id + "]";
	}
	
	public void compile() {
		System.out.println("Compiling");
	}

}
