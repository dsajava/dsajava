package construction;

/**
 * <code>CreditCheck</code> is abstract class for Credit Check.
 * 
 * @author diptanealroy
 * @category designPatterns
 * 
 * Book: Design Patterns in Java by Steven J Metsker, William C Wake
 * Intent: To help people new to Design Patterns
 */

abstract class CreditCheck {
	protected int id;

	double creditLimit(int id) {
		return 0;
	}
}

/**
 * An <code>CreditCheckOnline</code> object represents an credit check option when 
 * the credit agency is online
 * @author diptanealroy
 * @version 1.0
 *
 */
class CreditCheckOnline extends CreditCheck {

	/**
	 * Returns creditLimit in online mode
	 * @param id of the customer
	 * @return credit Limit in dollars
	 */
	public double creditLimit( int id ) {
		if ( id != 420 ) {
			return 1000000; // 1MM
		} else {
			return 0;
		}
	}
}

/**
 * An <code>CreditCheckOffline</code> object represents an credit check option when 
 * the credit agency is offline
 * @author diptanealroy
 * @version 1.0
 *
 */
class CreditCheckOffline extends CreditCheck {

	/**
	 * Returns creditLimit in offline mode
	 * @param id of the customer
	 * @return credit Limit in dollars
	 */
	public double creditLimit( int id ) {
		if (id != 420 )
			return 500000; //500K
		else {
			return 0;
		}
	}
}

/**
 * <code>Factory Class</code> is class for implementing Factory Design Pattern.
 * 
 * Intent of factory method is to let the class developer define the interface
 * for creating an object while retaining control of which class to instantiate.
 * 
 * Factory method pattern relieves a client from the burden of knowing which class
 * to instantiate.
 * 
 * For instance Iterator is a Factory method. The class of the iterator that a client
 * needs depends on the type of collection that the client wants to walk through.


 * @author diptanealroy
 *
 */
class FactoryClass {
	public static boolean isAgencyUp () {
		return false;
	}
	
	// Factory Method - creates concrete CreditCheck class based on agency status
	public static CreditCheck createCreditCheck() {
		if ( isAgencyUp() ) {
			return new CreditCheckOnline();
		}
		else {
			return new CreditCheckOffline();
		}
	}	
}

// Client or the user
public class FactoryPatternImpl {

	public static void main(String[] args) {
		CreditCheck cc = FactoryClass.createCreditCheck();
		System.out.println("Credit limit sanctioned is : " + cc.creditLimit(777));
		System.out.println(cc.getClass().getName()); // Offline credit
	}
}

